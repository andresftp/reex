<?php
if($tipo_menu == "Propuestas"){  
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">

  <!-- fwtable fwsrc="1-propuestas.png" fwbase="1-propuestas.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->

  <tr>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="119" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="104" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="8" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="184" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="8" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="203" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="9" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="104" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="61" height="1" id="undefined_2" border="0" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="1" id="undefined_2" border="0" /></td>

  </tr>

  <tr>

    <td rowspan="3"><a href="../procesos/propuestas.php"><img name="n1propuestas_r1_c1" src="../images/business_man_blue.png" width="119" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>

    <td colspan="7"><img name="n1propuestas_r1_c2" src="../images/1-propuestas_r1_c2.jpg" width="620" height="31" border="0" id="n1propuestas_r1_c2" alt="" /></td>

    <td rowspan="3"><img name="n1propuestas_r1_c9" src="../images/1-propuestas_r1_c9.jpg" width="61" height="100" border="0" id="n1propuestas_r1_c9" alt="" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="31" id="undefined_2" border="0" /></td>

  </tr>

  <tr>

    <td> <div style="width:220px;"><a href="../procesos/propuestas.php" class="button" style="font-size: 16px;" > Propuestas </a></div></td>

    <td><img name="n1propuestas_r2_c3" src="../images/1-propuestas_r2_c3.jpg" width="8" height="37" border="0" id="n1propuestas_r2_c3" alt="" /></td>

    <td><div style="width:290px;"><a href="../procesos/propuestaok.php" class="button" style="font-size: 16px;" > Propuestas aprobadas </a></div></td>

    <td><img name="n1propuestas_r2_c5" src="../images/1-propuestas_r2_c5.jpg" width="8" height="37" border="0" id="n1propuestas_r2_c5" alt="" /></td>

<td><div style="width:310px;"><a href="../procesos/propuestasnook.php" class="button" style="font-size: 16px;" > Propuestas no aprobadas </a></div></td>
    
    <td><img name="n1propuestas_r2_c7" src="../images/1-propuestas_r2_c7.jpg" width="9" height="37" border="0" id="n1propuestas_r2_c7" alt="" /></td>
	<td><div style="width:290px;"><a href="../parametros/clientes.php" class="button" style="font-size: 16px;" > Clientes </a></div></td>
    

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="8" height="37" id="undefined_2" border="0" /></td>

  </tr>

  <tr>

    <td colspan="7"><img name="n1propuestas_r3_c2" src="../images/1-propuestas_r3_c2.jpg" width="620" height="32" border="0" id="n1propuestas_r3_c2" alt="" /></td>

    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="32" id="undefined_2" border="0" /></td>

  </tr>

</table>
<?php
} else if ($tipo_menu == "Parametrizacion"){  

?>
<div class="container">
  <div class="row">
    <div class="col-md-1">
     <a href="../procesos/parametros.php"><img name="n1propuestas_r1_c1" src="../images/package_settings.png" width="100" height="96" border="0" id="n1propuestas_r1_c1" alt="" /></a>
    </div>
    <div class="col-md-11">
      <div class="row">
        <div class="col-md-12" style="padding-top:16px;">
        <a href="../parametros/tiposervicio.php" class="button" style="font-size: 16px;" > Servicios </a>
        <a href="../parametros/arp.php" class="button" style="font-size: 16px;" > ARL </a>
        <a href="../parametros/tiporden.php" class="button" style="font-size: 16px;" > Tipo de orden </a>
        <a href="../parametros/documentos.php" class="button" style="font-size: 16px;" > Documentos </a>
        <a href="../parametros/clientes.php" class="button" style="font-size: 16px;" > Clientes </a>    
     </div>
      </div>
      <div class="row">
        <div class="col-md-12" style="padding-top:26px;">
          <a href="../parametros/ciudad.php" class="button" style="font-size: 16px;" > Ciudades </a>
          <a href="../parametros/tarconsultor.php" class="button" style="font-size: 16px;" > Tarifa profesional </a>    
        
          <?php if($id_tipo != 2){ ?>
          <a href="../parametros/tararp.php" class="button" style="font-size: 16px;" > Tarifa clientes </a> 
        
          <a href="../parametros/cargos.php" class="button" style="font-size: 16px;" > Cargos </a>
          <a href="../parametros/sistema.php" class="button" style="font-size: 16px;" > Sistema </a>
           <?php } ?>
        </div>
        <div class="col-md-12" style="padding-top:26px; padding-bottom:16px;">
          <a href="../parametros/formatosarl.php" class="button" style="font-size: 16px;" > Formatos ARL </a>
		  <a href="../parametros/admincartelera.php" class="button" style="font-size: 16px;" > Cartelera de bienvenida </a>
          <a href="../parametros/bitacora.php" class="button" style="font-size: 16px;" > Bitacora </a>
            <a href="../parametros/sipab.php" class="button" style="font-size: 16px;" > Codigos Sipab </a>
        </div>
      </div>        
    </div>
  </div>
</div>
<?php
} else if ($tipo_menu == "Profesionales"){
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">
    <!-- fwtable fwsrc="4-consultores.png" fwbase="4-consultores.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->
    <tr>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="118" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="218" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="10" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="104" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="10" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="104" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="10" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="146" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="80" height="1" id="undefined_4" border="0" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="1" height="1" id="undefined_4" border="0" /></td>
    </tr>
    <tr>
      <td rowspan="3">
      <a href="consultores.php"><img name="n1propuestas_r1_c1" src="../images/users.png" width="118" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>
      <td colspan="7"><img name="n4consultores_r1_c2" src="../images/4-consultores_r1_c2.jpg" width="602" height="31" border="0" id="n4consultores_r1_c2" alt="" /></td>
      <td rowspan="3"><img name="n4consultores_r1_c9" src="../images/4-consultores_r1_c9.jpg" width="80" height="100" border="0" id="n4consultores_r1_c9" alt="" /></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="1" height="31" id="undefined_4" border="0" /></td>
    </tr>
    <tr>
     <?php
    if ($id_tipo==4 or $id_tipo==1)
 		{
		?>
      <td><div style="width:320px;"><a href="../procesos/planeacion.php" class="button" style="font-size: 16px;" > Asignaci&oacute;n de actividades </a></div></td> 
      <td><div style="width:180px;"><a href="../parametros/cargos.php" class="button" style="font-size: 16px;" > Cargos </a></div></td>
      <td><div style="width:180px;"><a href="../parametros/tarconsultor.php" class="button" style="font-size: 16px;" > Tarifas </a></div></td><?php } ?>
            <td><div style="width:280px;"><a href="../parametros/sersocial.php" class="button" style="font-size: 16px;" > Seguridad social </a></div></td>

      
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="1" height="36" id="undefined_4" border="0" /></td>
    </tr>
    
    <tr>
      <td colspan="7"></td>
      <td><img src="../images/spacer.gif" alt="" name="undefined_4" width="1" height="33" id="undefined_4" border="0" /></td>
    </tr>
</table>
<?php
} else if ($tipo_menu == "Planeacion"){
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <!-- fwtable fwsrc="3-planeacion.png" fwbase="3-planeacion.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->
  <tr>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="118" height="1" id="undefined_2" border="0" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="224" height="1" id="undefined_2" border="0" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="15" height="1" id="undefined_2" border="0" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="115" height="1" id="undefined_2" border="0" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="328" height="1" id="undefined_2" border="0" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="1" id="undefined_2" border="0" /></td>
  </tr>
  <tr>
    <td rowspan="3"><a href="../procesos/planeacion.php"><img name="n1propuestas_r1_c1" src="../images/dispatch_order_up.png" width="118" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>
    <td colspan="3"><img name="n3planeacion_r1_c2" src="../images/3-planeacion_r1_c2.jpg" width="354" height="30" border="0" id="n3planeacion_r1_c2" alt="" /></td>
    <td rowspan="3"><img name="n3planeacion_r1_c5" src="../images/3-planeacion_r1_c5.jpg" width="328" height="100" border="0" id="n3planeacion_r1_c5" alt="" /></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="30" id="undefined_2" border="0" /></td>
  </tr>
  <tr>
    <td><div style="width:315px;"><a href="../procesos/planeacion_difhoras.php" class="button" style="font-size: 16px;" > Asignaciones incompletas </a></div></td>
    <td><img name="n3planeacion_r2_c3" src="../images/3-planeacion_r2_c3.jpg" width="15" height="38" border="0" id="n3planeacion_r2_c3" alt="" /></td>
    <td><div style="width:220px;"><a href="../procesos/consultores.php" class="button" style="font-size: 16px;" > Profesionales </a></div></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="38" id="undefined_2" border="0" /></td>
  </tr>
  <tr>
    <td colspan="3"></td>
    <td><img src="../images/spacer.gif" alt="" name="undefined_2" width="1" height="32" id="undefined_2" border="0" /></td>
  </tr>
</table>
<?php
} else if ($tipo_menu == "Actividades"){
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <!-- fwtable fwsrc="5-actividades.png" fwbase="5-actividades.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->
  <tr>
    <td><img src="../images/spacer.gif" width="121" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="217" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="19" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="217" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="19" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="132" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="19" height="1" border="0" alt="" /></td>
	<td><img src="../images/spacer.gif" width="1" height="1" border="0" alt="" /></td>

  </tr>
  <tr>
    <td rowspan="5"><a href="actividades.php"><img name="n1propuestas_r1_c1" src="../images/kivio.png" width="121" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>
    <td colspan="5"><img name="n5actividades_r1_c2" src="../images/5-actividades_r1_c2.jpg" width="601" height="9" border="0" id="n5actividades_r1_c2" alt="" /></td>
    <!--<td rowspan="5"><img name="n5actividades_r1_c7" src="../images/5-actividades_r1_c7.jpg" width="50" height="100" border="0" id="n5actividades_r1_c7" alt="" /></td>-->
    <td><img src="../images/spacer.gif" width="1" height="9" border="0" alt="" /></td>
  </tr>
  <tr>
    <td><div style="width:290px;"><a href="act_aprobadas.php" class="button" style="font-size: 16px;" > Actividades aprobadas </a></div>
    </td>
    <td><img name="n5actividades_r2_c3" src="../images/5-actividades_r2_c3.jpg" width="16" height="38" border="0" id="n5actividades_r2_c3" alt="" /></td>
    <td><div style="width:311px;"><a href="act_noaprobadas.php" class="button" style="font-size: 16px;" > Actividades no aprobadas </a></div>
    </td>
    <td><img name="n5actividades_r2_c5" src="../images/5-actividades_r2_c5.jpg" width="19" height="38" border="0" id="n5actividades_r2_c5" alt="" /></td>
		<td><div style="width:200px;"><a href="controlviaticos.php" class="button" style="font-size: 16px;" > Viaticos </a></div></td>
    <td><img name="n5actividades_r2_c5" src="../images/5-actividades_r2_c5.jpg" width="19" height="38" border="0" id="n5actividades_r2_c5" alt="" /></td>
		
	  <td><img src="../images/spacer.gif" width="1" height="50" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="5"><img name="n5actividades_r3_c2" src="../images/5-actividades_r3_c2.jpg" width="601" height="28" border="0" id="n5actividades_r3_c2" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="8" border="0" alt="" /></td>
  </tr>
  <tr>
    <td><div style="width:311px;"><a href="controlviaticos_apro.php" class="button" style="font-size: 16px;" > Viaticos aprobados </a></div></td>
    <td><img name="n5actividades_r4_c3" src="../images/5-actividades_r4_c3.jpg" width="16" height="37" border="0" id="n5actividades_r4_c3" alt="" /></td>
    <td><div style="width:311px;"><a href="controlviaticos_noapro.php" class="button" style="font-size: 16px;" > Viaticos no aprobados </a></div></td>
    <td><img name="n5actividades_r4_c5" src="../images/5-actividades_r4_c5.jpg" width="19" height="37" border="0" id="n5actividades_r4_c5" alt="" /></td>
    <td><div style="width:280px;"><a href="act_pagoscobros.php" class="button" style="font-size: 16px;" > Facturaci&oacute;n </a></div></td>
    <td><img src="../images/spacer.gif" width="1" height="37" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="5"></td>
    <td><img src="../images/spacer.gif" width="1" height="8" border="0" alt="" /></td>
  </tr>
</table>
<?php
} else if ($tipo_menu == "Ordenes de servicio"){
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <!-- fwtable fwsrc="2-orden.png" fwbase="2-orden.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->
  <tr>
    <td><img src="../images/spacer.gif" width="119" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="206" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="16" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="147" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="16" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="104" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="192" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
  </tr>
  <tr>
    <td rowspan="3"><a href="ordenservicio.php"><img name="n1propuestas_r1_c1" src="../images/activity_monitor.png" width="121" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>
    <td colspan="5"><img name="n2orden_r1_c2" src="../images/2-orden_r1_c2.jpg" width="489" height="31" border="0" id="n2orden_r1_c2" alt="" /></td>
    <td rowspan="3"><img name="n2orden_r1_c7" src="../images/2-orden_r1_c7.jpg" width="192" height="100" border="0" id="n2orden_r1_c7" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="31" border="0" alt="" /></td>
  </tr>
  <tr>
    <td><div style="width:300px;"><a href="ordenserviciocre.php" class="button" style="font-size: 16px;" > Nueva orden de servicio </a></div>
</td>
    <td><img name="n2orden_r2_c3" src="../images/2-orden_r2_c3.jpg" width="16" height="36" border="0" id="n2orden_r2_c3" alt="" /></td>
    <td><div style="width:250px;"><a href="ordenservicio_prov.php" class="button" style="font-size: 16px;" > OS provisionales </a></div></td>
    <td><img name="n2orden_r2_c5" src="../images/2-orden_r2_c5.jpg" width="16" height="36" border="0" id="n2orden_r2_c5" alt="" /></td>
    <td><div style="width:250px;"><a href="../parametros/clientes.php" class="button" style="font-size: 16px;" > Clientes </a></div></td>
    <td><img src="../images/spacer.gif" width="1" height="36" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="5"></td>
    <td><img src="../images/spacer.gif" width="1" height="33" border="0" alt="" /></td>
  </tr>
</table>
<?php
} else if ($tipo_menu == "Administrativa"){
?>
<table border="0" cellpadding="0" cellspacing="0" width="800">
  <!-- fwtable fwsrc="6-administrativa.png" fwbase="6-administrativa.jpg" fwstyle="Dreamweaver" fwdocid = "800861674" fwnested="0" -->
  <tr>
    <td><img src="../images/spacer.gif" width="119" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="134" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="14" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="57" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="10" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="65" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="106" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="12" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="283" height="1" border="0" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="1" border="0" alt="" /></td>
  </tr>
  <tr>
    <td rowspan="5"><a href="administrativa.php"><img name="n1propuestas_r1_c1" src="../images/system.png" width="121" height="100" border="0" id="n1propuestas_r1_c1" alt="" /></a></td>
    <td colspan="8"><img name="n6administrativa_r1_c2" src="../images/6-administrativa_r1_c2.jpg" width="681" height="9" border="0" id="n6administrativa_r1_c2" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="9" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="3"><div style="width:300px;"><a href="propuestaok_admin.php" class="button" style="font-size: 16px;" > Propuestas aprobadas </a></div></td>
    <td><img name="n6administrativa_r2_c5" src="../images/6-administrativa_r2_c5.jpg" width="10" height="38" border="0" id="n6administrativa_r2_c5" alt="" /></td>
    <td colspan="2"><div style="width:270px;"><a href="ordenserviciocre_admin.php" class="button" style="font-size: 16px;" > Ordenes de servicio </a></div></td>
    <td><img name="n6administrativa_r2_c8" src="../images/6-administrativa_r2_c8.jpg" width="12" height="38" border="0" id="n6administrativa_r2_c8" alt="" /></td>
    <td><div style="width:270px;"><a href="ordenservicio_prov_admin.php" class="button" style="font-size: 16px;" > OS provisionales</a></div></td>
    <td><img src="../images/spacer.gif" width="1" height="38" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="8"></td>
    <td><img src="../images/spacer.gif" width="1" height="8" border="0" alt="" /></td>
  </tr>
  <tr>
    <td><div style="width:270px;"><a href="act_pagoscobros_adm.php" class="button" style="font-size: 16px;" > Facturaci&oacute;n </a></div></td>
    <td><img name="n6administrativa_r4_c3" src="../images/6-administrativa_r4_c3.jpg" width="14" height="56" border="0" id="n6administrativa_r4_c3" alt="" /></td>
    <td colspan="3"><div style="width:180px;"><a href="viaticos_admin.php" class="button" style="font-size: 16px;" > Viaticos </a></div></td>
    <td rowspan="2" colspan="3"><img name="n6administrativa_r4_c7" src="../images/6-administrativa_r4_c7.jpg" width="401" height="45" border="0" id="n6administrativa_r4_c7" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="36" border="0" alt="" /></td>
  </tr>
  <tr>
    <td colspan="5"><img name="n6administrativa_r5_c2" src="../images/6-administrativa_r5_c2.jpg" width="280" height="9" border="0" id="n6administrativa_r5_c2" alt="" /></td>
    <td><img src="../images/spacer.gif" width="1" height="9" border="0" alt="" /></td>
  </tr>
</table>
<?php
} 
?>