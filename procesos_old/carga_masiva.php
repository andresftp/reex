<?php
include_once ('clase/cargaMasiva.php');
include_once ('../include/conexion.php');
header ('Content-type: text/html; charset=utf-8');
$objConexion = new Conectar();
$objCarga= new cargaMasiva($objConexion);
// ponemos el tiempo en infinito
date_default_timezone_set('America/Bogota');
set_time_limit(0);

if(isset($_POST)){
    if($_GET['acc']=='ux'){
        //Verificar el nit ingresado
        $resultNit = $objCarga->checkNit($_POST['nitcli']);
       if(count($resultNit)>0){
           ?>
             <script language="javascript">
                 alert("El nit ingresado ya está registrado");
                 location.href = "carga_masiva.php";
             </script>
           <?php
       }else{
           //registramos el cliente en la tabla original
           $objCarga->insertaCliente($_POST['id_cliente'],$_POST);
           ?>
           <script language="javascript">
               alert("Cliente actualizado con exito");
               location.href = "carga_masiva.php";
           </script>
           <?php

       }
    }

    if( isset($_FILES['ordenFile']) ){
        $nombre_archivo 	= 	str_replace(" ","_",$_FILES['ordenFile']['name']);

        if( strlen($nombre_archivo) >0 ){
            $nombre_archivo		=	strftime( "%Y-%m-%d", time())."_".intval (rand()).substr($nombre_archivo,strrpos($nombre_archivo,"."));

            //$tipo_archivo 		= 	$_FILES['farchivo']['type'];

            $tamano_archivo 	= 	$_FILES['ordenFile']['size'];

            $ruta_upload		=	"../tmp/".$nombre_archivo;
            $formato = explode('.',$nombre_archivo);
            $ext = $formato[1];
            if($ext=='csv') {
                if ($tamano_archivo > 0) {

                    if (move_uploaded_file($_FILES['ordenFile']['tmp_name'], $ruta_upload)) {
                        $archivo = "../tmp/$nombre_archivo";
                        $resultCarga = $objCarga->registrarCargaMasiva($archivo, $_POST['tipoArl']);
                        //Verificamos que el archivo es correcto y empezamos a migrar las ordenes
                        $resultCarga = explode(':', $resultCarga);
                        if ($resultCarga[0] == 'success') {
                            //traemos las ordenes temporales
                            $rsAllOrder = $objCarga->obtenerOrdenTemporal();
                            $cc = 0;
                            for ($i = 0; $i < count($rsAllOrder); $i++) {
                                //verificamos la arl
                                if ($_POST['tipoArl'] == 'sura') {
                                    //verificamos si el número de orden aún no esta registrado
                                    $resulChk = $objCarga->checkOrdenSura($rsAllOrder[$i]->numorden);
                                    if (count($resulChk) == 0) {
                                        $objCarga->migrarOrden($rsAllOrder[$i]->id_ordenservicio);
                                        $cc = $cc + 1;
                                    }

                                } elseif ($_POST['tipoArl'] == 'bolivar') {
                                    $resulChk = $objCarga->checkOrdenBolivar($rsAllOrder[$i]->numorden, $rsAllOrder[$i]->secuencia, $rsAllOrder[$i]->cronograma);
                                    if (count($resulChk) == 0) {
                                        $objCarga->migrarOrden($rsAllOrder[$i]->id_ordenservicio);
                                        $cc = $cc + 1;
                                    }

                                }
                            }
                        }

                        ?>
                        <script language="javascript">
                            <?php
                            if($resultCarga[0] == 'success'){

                            ?>
                            alert('Se han cargado <?php echo $cc ?> ordenes correctamente');
                            location.href = "ordenserviciocre.php";
                            <?php
                            }else{
                            ?>
                            alert('<?php echo $resultCarga[1] ?>');
                            <?php
                            }
                            ?>
                        </script>
                        <?php
                    } else {

                        $Rmsg = 2;

                    }

                }

            }
            else{
                ?>
                <script language="javascript">

                    alert('El formato del archivo seleccionado no es CSV.');
                </script>
            <?php

            }
        }else{

            $Rmsg=1;

        }

    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <script src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/moment.js"></script>
    <script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <!--Hoja de estilos del calendario -->
    <link rel="stylesheet" type="text/css" media="all" href="../include/nuevocalendar/calendar-blue2.css" title="win2k-cold-1" />
    <!-- librer�a principal del calendario -->
    <script type="text/javascript" src="../include/nuevocalendar/calendar.js"></script>
    <!-- librer�a para cargar el lenguaje deseado -->
    <script type="text/javascript" src="../include/nuevocalendar/lang/calendar-es.js"></script>
    <!-- librer�a que declara la funci�n Calendar.setup, que ayuda a generar un calendario en unas pocas l�neas de c�digo -->
    <script type="text/javascript" src="../include/nuevocalendar/calendar-setup.js"></script>
    <title>Administracion Clientes</title>
    <link href="../css/reex.css" rel="stylesheet" type="text/css" />
    <!--<script language="JavaScript" type="text/javascript" src="ajax_admactor.js"></script>-->
    <script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/funciones.js"></script>
    <link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
    <link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
    <style type="text/css">

        td img {display: block;}td img {display: block;}
    </style>
</head>



<body onload="MM_preloadImages('../images/2-orden_r2_c2_f2.jpg','../images/2-orden_r2_c4_f2.jpg','../images/2-orden_r2_c6_f2.jpg')">
<table width="85%" border="0" align="center">
    <tr>
        <td><?php
            $tipo_menu = "Ordenes de servicio";
            include_once ("../include/menu_top.php");?></td>
    </tr>
    <tr>
        <td align="center">
            <style>
                .borderTable{
                    border: solid 2px #3F658C;
                    border-radius: 9px;
                    width: 60%;
                    padding: 15px;
                }
            </style>
            <br><br>
            <div class="borderTable">
                <form action="<?php $_SERVER['PHP_SELF'] ?>" method="post" enctype="multipart/form-data">
                    <table width="100%" border="0">
                        <tr>
                            <td align="center" colspan="2">
                                <div class="msj">
                                    Para la carga masiva, debe seleccionar el archivo descargado tal cual del sistema de Bolivar o de Sura y luego guardarlo en el siguiente
                                    formato <b>CSV (delimitado por comas)(*.csv)</b> antes de subirlo al sistema.
                                </div>
                                <br>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td class="menuTil">Arl</td>
                            <td>
                                <select name="tipoArl" style="width: 300px;" required>
                                    <option value="">-Seleccione uno-</option>
                                    <option value="sura">Sura</option>
                                    <option value="bolivar">Bolivar</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="menuTil">
                                CSV de carga
                            </td>
                            <td>
                                <input style="width: 300px;" type="file" name="ordenFile" accept=".csv" required>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input type="submit" name="cargar" value="Cargar Ordenes">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tr>
                    <td>
                        <input type="submit" name="ncliente" value="Nuevos clientes Registrados" onclick="window.location='carga_masiva.php?tipoTable=cNew'">
                    </td>
                    <td>
                        <input type="submit" name="rcliente" value="Clientes por revisar" onclick="window.location='carga_masiva.php?tipoTable=cReview'">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="menutitulo"><?php if($_GET['tipoTable']=='cNew'){
            echo 'Nuevos clientes registrados';
            }
            elseif($_GET['tipoTable']=='cReview'){
            echo 'Clientes por revisar';
            }?></td>
    </tr>
    <tr>
        <td>
            <?php
            if($_GET['acc'] =='u'){
                //traemos la información temporal del cliente
                $resultCliente = $objCarga->getClientById($_GET['id']);
                ?>
                <form method="post" action="?acc=<?php echo $_GET['acc']?>x">
                <table width="100%">
                    <tr>
                        <td class="menuTil">Nombre o Raz&oacute;n Social</td>
                        <td><input name="nombrecli" type="text" id="nombrecli" size="30" value="<?php echo $resultCliente[0]->nom_cliente;?>" required/></td>
                    </tr>
                    <tr>
                        <td class="menuTil">NIT</td>
                        <td><input name="nitcli" type="text" id="nitcli" size="15" required></td>
                    </tr>
                    <tr>
                        <td class="menuTil">Direcci&oacute;n</td>
                        <td><input name="dircli" type="text" id="dircli" size="30"/></td>
                    </tr>
                    <tr>
                        <td class="menuTil">Tel&eacute;fono</td>
                        <td><input name="telcli" type="text" id="telcli" size="30"/></td>
                    </tr>
                    <tr>
                        <td class="menuTil">Actividad Econ&oacute;mica Principal</td>
                        <td><input name="actcli" type="text" id="actcli" size="50"/></td>
                    </tr>
                    <tr>
                        <td class="menuTil">Ciudad</td>
                        <td><?php
                            echo "<select  name='sel_ciudad' id='sel_ciudad'  class='contenidoTabla2'>
	            <option value='0'>-Seleccione Ciudad-</option>";

                            $consulta_sql2="SELECT * FROM  tb_ciudad ORDER BY nom_ciudad";
                            $sql2=mysql_query($consulta_sql2);
                            while($registro2=mysql_fetch_row($sql2))
                            {
                                $nombrec = $registro2[1];
                                if($registro2[0] == $row1['ciudad_id'])
                                {
                                    echo "<option value='".$registro2[0]."' selected='selected'>".substr(ucfirst($nombrec),0,50)."</option>";
                                }
                                else
                                {
                                    echo "<option value='".$registro2[0]."'>".substr(ucfirst($nombrec),0,50)."</option>";
                                }

                            }
                            echo " </select>"; ?></td>
                    </tr>
                    <tr>

                        <td width="23%" class="menuTil">Observaciones</td>

                        <td width="77%">

                            <textarea name="obscli" id="obscli" cols="45" rows="5"><?php echo $row1['observa'];?></textarea>

                          <input name="id_cliente" type="hidden" id="id_cliente" size="30" value="<?php echo $_GET['id'];?>"/>


                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <input type="submit" name="guardaC" value="Guardar Cliente">

                        </td>
                    </tr>
                </table>
                </form>
                <?php
            }else {
                ?>
                <table border=0 width="100%" style="border:1px solid #FF0000; color:#3F658C;width:100%;">
                    <tr>
                        <td width="2%" align="center" class="titulotablacampo2">#</td>
                        <td width="40%" align="center" class="titulotablacampo2">Nombre o Razon Social</td>
                        <td width="40%" align="center" class="titulotablacampo2">Nit</td>
                        <td width="18%" align="center" class="titulotablacampo2">Opción</td>
                    </tr>
                    <?php
                    if ($_GET['tipoTable'] == 'cReview') {
                        //Traemos los nuevs clientes registrados
                        $rsAllClient = $objCarga->getClientRev();

                        for ($c = 0; $c < count($rsAllClient); $c++) {
                            echo '<tr>
                                  <td>' . $c . '</td>
                                  <td>' . $rsAllClient[$c]->nom_cliente . '</td>
                                  <td>' . $rsAllClient[$c]->nit . '</td>
                                  <td>
                                   <a href="?acc=u&id=' . $rsAllClient[$c]->id_cliente . '">
                                  <img src="../imagenes/modificar1.jpg" width="20" height="20" border="0" title="Modificar Cliente">
                                  </a>
                                  </td>
                              </tr>';
                        }

                    } else {
                        //Traemos los nuevs clientes registrados
                        $rsAllClient = $objCarga->getClientNew();

                        for ($c = 0; $c < count($rsAllClient); $c++) {
                            echo '<tr>
                                  <td>' . $c . '</td>
                                  <td>' . $rsAllClient[$c]->nom_cliente . '</td>
                                  <td>' . $rsAllClient[$c]->nit . '</td>
                                  <td>
                                    <a href="../parametros/clientes.php?mod=mod&id=' . $rsAllClient[$c]->id_cliente . '">                                  <img src="../imagenes/modificar1.jpg" width="20" height="20" border="0" title="Modificar Cliente">

                                  <img src="../imagenes/modificar1.jpg" width="20" height="20" border="0" title="Modificar Cliente">
                                  </a>
                                  </td>
                              </tr>';
                        }
                    }
                    ?>
                </table>
                <?php
            }
            ?>
        </td>
    </tr>
</table>
</body>
</html>