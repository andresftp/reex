<?php
#ini_set('display_errors', 1);error_reporting(E_ALL);
// Cargamos la librería dompdf que hemos instalado en la carpeta dompdf
date_default_timezone_set('America/Bogota');
require_once 'clase/dompdf/lib/html5lib/Parser.php';
require_once 'clase/dompdf/src/Autoloader.php';
include_once ("../include/conexion.php");

Dompdf\Autoloader::register();

// reference the Dompdf namespace
use Dompdf\Dompdf;


$objConexion = new Conectar();


//Obtener información de la actividad
$consulta_sqlact="SELECT a.id_asignacion, p.nom_arp, c.nom_cliente, u.nom_usuario, a.horas_asg, a.viaticos_asg, a.observaciones, 
                    s.nom_tiposervicio, c.nit, c.direccion, c.telefono, c.actividade, d.nom_ciudad, o.contacto_cli, o.secuencia, 
                    o.cronograma, o.usuariocliente_id, p.id_arp, o.tiposervicio1_id, o.tiposervicio2_id, o.tiposervicio3_id, 
                    o.tiposervicio4_id, o.tiposervicio5_id, o.f_inicio, o.f_termina, o.num_horas, o.viaticos, o.desc_viaticos, 
                    m.nom_sistema, o.detalle, o.obs_ppta, o.numorden, o.provisional, o.asesor_arl, a.acta_bolivar, o.tema_bolivar, 
                    o.sipab_bolivar AS code_sipab, p.nom_arp, o.arp_id, o.fecha_elabora, car.nom_cargo, sip.actividad
                  FROM tb_asignacion a 
                  INNER JOIN tb_ordenservicio o ON (o.id_ordenservicio=a.ordenservicio_id) 
                  INNER JOIN tb_cliente c ON (c.id_cliente=o.cliente_id) 
                  INNER JOIN tb_usuario u ON (u.id=a.usuario_id) 
                  INNER JOIN tb_cargo car ON (car.id_cargo=u.cargo_id) 
                  INNER JOIN tb_tiposervicio s ON (a.tiposervicio_id=s.id_tiposervicio) 
                  INNER JOIN tb_sistema m ON (m.id_sistema=o.sistema) 
                  INNER JOIN tb_ciudad d ON (d.id_ciudad=c.ciudad_id) 
                  INNER JOIN tb_arp p ON (p.id_arp=o.arp_id) 
                  LEFT JOIN tb_sipab sip ON (sip.codigo=o.sipab_bolivar) 
                  WHERE a.estado=1 AND a.id_asignacion=".$_GET['id'];

$sqlact=mysql_query($consulta_sqlact);
$rowo1=mysql_fetch_array($sqlact);

//Año, mes día
$fecha_elabora = explode('-',$rowo1['fecha_elabora']);
$anoEla = $fecha_elabora[0];
$mesEla = $fecha_elabora[1];
$diaEla = $fecha_elabora[2];

//Define el tipo de actividad
if($rowo1['tiposervicio1_id']==1){
    $tipo_act = 'A';
}elseif ($rowo1['tiposervicio2_id']==2){
    $tipo_act = 'C';
}
elseif ($rowo1['tiposervicio4_id']==4){
    $tipo_act = 'T';
}
elseif ($rowo1['tiposervicio5_id']==5){
    $tipo_act = 'E';
}

// instantiate and use the dompdf class
$html = '<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>AT-031</title>
</head>
<style type="text/css">
    body{
        font-family: cabin, sans-serif;
        font-size: 10px;
    }
	.tabla_fija{
       table-layout:fixed;
    }	
    .td_bordes_arriba{
        color: #000;
		border-radius:6px;
		border-top: solid 1px #000;
       	border-left: solid 1px #000;
		border-right: solid 1px #000;
		padding: 5px;		
	
    }
	.td_bordes_abajo{
        color: #000;
        border-radius:6px;
		border:solid 1px #000;
		padding: 5px;
    }
    .company {
        border-collapse: separate;
        border-spacing: 0;
    }
    .company tr th,
    .company tr td {
        border-right: 1px solid #000;
        border-bottom: 1px solid #000;
        padding: 5px;
    }
    .company tr th:first-child,
    .company tr td:first-child {
        border-left: 1px solid #000;
    }
    .company tr th {
        font-weight: normal;
        border-top: 1px solid #000;
        text-align: left;
    }

    /* top-left border-radius */
    .company tr:first-child th:first-child {
        border-top-left-radius: 6px;
    }

    /* top-right border-radius */
    .company tr:first-child th:last-child {
        border-top-right-radius: 6px;
    }

    /* bottom-left border-radius */
    .company tr:last-child td:first-child {
        border-bottom-left-radius: 6px;
    }

    /* bottom-right border-radius */
    .company tr:last-child td:last-child {
        border-bottom-right-radius: 6px;
    }

    .rotate {

        /* Safari */
        -webkit-transform: rotate(-90deg);

        /* Firefox */
        -moz-transform: rotate(-90deg);

        /* IE */
        -ms-transform: rotate(-90deg);

        /* Opera */
        -o-transform: rotate(-90deg);

        /* Internet Explorer */
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);

    }

</style>
<body>
<table width="750px" cellspacing="0">
        <tr>
            <td>
                <table class="tabla_fija" width="100%" border="0">
                    <!--  Logos y titulo del documento -->
                    <tr>
                        <td width="95%">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <img src="../imagenes/arl_bolivar.png">
                                    </td>
                                    <td align="right">
                                        <img src="../imagenes/banner_bolivar.png">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="font-weight: bold;font-size: 19px">
                                        SEGUIMIENTO DE REUNIONES Y ACTIVIDADES
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10%"></td>
                    </tr>
                    <!-- información básica del documento -->
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="20%">
                                        <!-- Fecha -->
                                        <table border="0" width="100%" class="company">
                                            <tr>
                                                <th align="center">AÑO</th>
                                                <th align="center">MES</th>
                                                <th align="center">DíA</th>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    '.$anoEla.'
                                                </td>
                                                <td align="center">
                                                    '.$mesEla.'
                                                </td>
                                                <td align="center">
                                                    '.$diaEla.'
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <!-- SIPAB -->
                                    <td width="30%">
                                        <table class="tabla_fija" width="100%">
                                            <tr>
                                                <td align="right">SIPAB No Cronograma</td>
                                                <td class="td_bordes_arriba" width="30%" align="center">'.$rowo1['cronograma'].'</td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    Secuencia
                                                </td>
                                                <td class="td_bordes_abajo" width="30%" align="center"> '.$rowo1['secuencia'].'</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <!--  Tipo actividad -->
                                    <td width="20%">
                                       <table class="tabla_fija" width="100%">
                                            
											<tr>
											 	<td width="20%" align="right"></td>
                                                <td class="td_bordes_arriba" width="80%" align="center">TIPO DE ACTIVIDAD</td>
                                            </tr>
                                            <tr>
												<td width="20%" align="right"></td>
                                                <td class="td_bordes_abajo" width="80%" align="center">'.$tipo_act.'</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20%">
                                        <table class="tabla_fija" width="100%">
                                            <tr>
                                                 <td  valign="top" class="td_bordes_abajo" width="70">No CONSECUTIVO<br>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                        <td>

                        </td>
                    </tr>
                    <!--  Dato del cliente-->
                    <tr>
                        <td>
                            <table class="company tabla_fija" border="0" width="100%">
                                <tr>
                                    <th width="30%" colspan="2">
                                        EMPRESA
                                        <br>
                                        '.$rowo1['nom_cliente'].'
                                    </th>
                                    <th width="20%" colspan="2">
                                        DIRECCIÓN
                                        <br>
                                         '.(($rowo1['nom_ciudad']=='')?'&nbsp;':$rowo1['nom_ciudad']).'
                                    </th>
                                    <th width="20%" colspan="2">
                                        NIT
                                        <br>
                                        '.$rowo1['nit'].'
                                    </th>
                                    <th width="30%" colspan="2">
                                        PÓLIZA
                                        <br>
                                         '.$rowo1['numorden'].'
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        CIUDAD/DEPTO
                                        <br>
                                        '.(($rowo1['nom_ciudad']=='')?'&nbsp;':$rowo1['nom_ciudad']).'
                                    </td>
                                    <td>
                                        TELÉFONOS
                                        <br>
                                        '.(($rowo1['telefono']=='')?'&nbsp;':$rowo1['telefono']).'
                                    </td>
                                    <td>
                                        PICAT PLUS
                                        <br>
                                        &nbsp;
                                    </td>
                                    <td>
                                        PECAT
                                        <br>
                                        &nbsp;
                                    </td>
                                    <td>
                                        OTRO ¿CUÁL?
                                        <br>
                                        &nbsp;
                                    </td>
                                    <td>
                                        HORA INICIO
                                        <br>
                                        &nbsp;
                                    </td>
                                    <td>
                                        HORA SALIDA
                                        <br>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        ASESOR DNPRL
                                        <br>
                                        '.(($rowo1['asesor_arl']=='')?'&nbsp;':$rowo1['asesor_arl']).'                                     
                                    </td>
                                    <td colspan="2">
                                        CÓDIGO PROVEEDOR
                                        <br>
                                        469
                                    </td>
                                    <td colspan="3">
                                        CÓDIGO PROFESIONAL PROVEEDOR
                                        <br>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        PARTICIPANTES ARL
                                    </td>
                                    <td colspan="4" align="center">
                                        PARTICIPANTES EMPRESA
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                       NOMBRES
                                    </td>
                                    <td colspan="2" align="center">
                                       CARGO
                                    </td>
                                    <td colspan="2" align="center">
                                       NOMBRES
                                    </td>
                                    <td colspan="2" align="center">
                                       CARGO
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                       '.$rowo1['nom_usuario'].'
                                    </td>
                                    <td colspan="2" align="center">
                                       '.$rowo1['nom_cargo'].'
                                    </td>
                                    <td colspan="2" align="center">
                                        &nbsp;
                                    </td>
                                    <td colspan="2" align="center">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td></td>
                    </tr>
                    <!-- Tema y detalle -->
                    <tr>
                        <td>
                            <table class="company" border="0" width="100%">
                                <tr>
                                    <th colspan="2" width="30%">
                                        TEMA A TRATAR
                                        <br>
                                        '.(($rowo1['actividad']=='')?'&nbsp;':$rowo1['actividad']).'    
                                    </th>
                                    <th colspan="7">
                                        PUNTOS TRATADOS
                                        <br>
                                        '.(($rowo1['detalle']=='')?'&nbsp;':$rowo1['detalle']).'  
                                    </th>
                                </tr>
                                <tr>
                                    <td width="15%">
                                       <b>COD. SIPAB</b>
                                    </td>
                                    <td colspan="5" width="50%">
                                        <b>DECISIONES Y/O COMPROMISOS ADQUIRIDOS</b>
                                    </td>
                                    <td colspan="2" width="20%">
                                        <b>RESPONSABLE (S)</b>
                                    </td>
                                    <td width="15%">
                                        <b>FECHA</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        '.(($rowo1['code_sipab']=='')?'&nbsp;':$rowo1['code_sipab']).'
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp; <br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                        &nbsp;
                                    </td>
                                    <td colspan="5" width="50%" valign="top">
                                    '.(($rowo1['detalle']=='')?'&nbsp;':$rowo1['detalle']).'
                                    </td>
                                    <td colspan="2" width="20%">

                                    </td>
                                    <td width="15%">

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="9">
                                        <b>OBSERVACIONES / SUGERENCIAS</b>
                                        <br>
                                         '.(($rowo1['obs_ppta']=='')?'&nbsp;':$rowo1['obs_ppta']).'
                                         <br>
                                        &nbsp;<br>
                                        &nbsp;<br>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" width="0%">
                                        PRÓXIMA REUNIÓN  &nbsp;&nbsp; &nbsp;&nbsp;SI___   &nbsp;&nbsp; &nbsp;&nbsp;NO___    &nbsp;&nbsp; &nbsp;&nbsp;TEMA
                                    </td>
                                    <td colspan="2" width="20%">
                                        FECHA
                                    </td>
                                    <td colspan="2" width="10%">
                                        HORA
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" valign="top">
                            <img src="../imagenes/ori_imp.PNG" width="25px">
                        </td>
                    </tr>
                    <!-- Calificación -->
                    <tr>
                        <td valign="top">
                            <table class="company" border="0" width="100%">
                                <tr>
                                    <th>
                                        Califique de 1 a 5 el presente servicio de la ARL Bolívar, en donde 1 es la más baja y
                                        5 la más alta. <br>
                                        1___ &nbsp;&nbsp; &nbsp;&nbsp; 2___&nbsp;&nbsp; &nbsp;&nbsp;3___&nbsp;&nbsp; &nbsp;&nbsp;
                                        4___ &nbsp;&nbsp; &nbsp;&nbsp; 5___&nbsp;&nbsp; &nbsp;&nbsp;
                                        <br>
                                        &nbsp;
                                    </th>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Su respuesta contribuye al mejoramiento coninul de nuestros servicios. Gracias
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top"><img src="../imagenes/formato_red.PNG" width="13px"></td>
                    </tr>
                    <!-- Firma -->
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td colspan="2" align="center">
                                        <b>
                                            SU FIRMA NOS ASEGURA QUE TODO LO CONSIGNADO EN ESTE FORMATO ES VERAZ
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        ______________________________________
                                    </td>
                                    <td align="center">
                                        ______________________________________
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        FIRMA ARL
                                    </td> <td align="center">
                                        FIRMA EMPRESA
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                      <td></td>
                    </tr>
                    <!-- Footer -->
                    <tr>
                        <td align="center">
                            <img src="../imagenes/footer.png">
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>';

#echo $html;
$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("AT-031.pdf", array("Attachment" => false));

exit(0);