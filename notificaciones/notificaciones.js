$(document).ready(function() {
	if  (!("Notification"  in  window))  {
		alert("Este navegador no soporta notificaciones de escritorio");
	}
	else  if  (Notification.permission  ===  "granted")  {
		$.ajax({
			data:  {proceso:1},
			url:   'notificaciones.php',
			type:  'post',
			success:  function (response) {
				var options  =   {
					body:   response,
					icon:   "../img/reex.PNG",
					dir :   "ltr"
				};
				var  notification  =  new  Notification("Actividades Nuevas", options);
			}
		});
	}
	else  if  (Notification.permission  !==  'denied')  {
		Notification.requestPermission(function (permission)  {
			if  (!('permission'  in  Notification))  {
				Notification.permission  =  permission;
			}
			if  (permission  ===  "granted")  {
				var  options  =   {
					body:   "Acceso no permitido",
					icon:   "../img/reex.PNG",
					dir :   "ltr"
				};
				var  notification  =  new  Notification("Calendario reex", options);
			}
		});
	}
});
