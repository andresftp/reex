<?php

class cargaMasiva{


    public $db;

    function __construct($bd) {
        $this->db = $bd;
    }


    //Cargar masiva a tabla temporal
    function registrarCargaMasiva($archivo,$arl){
        $tipoServicio = array(
          'C'=>'',
          'T'=>'',
          'A'=>'',
          'E'=>'',
        );
        if($arl=='bolivar'){
            $nField =41;
            $fieldRead = array(0,1,4,5,6,7,9,15,35,38,39);
            $fieldInsert = "nit_empresa, nombre_empresa, cronograma, secuencia, sipab_bolivar, tema_bolivar, num_horas, servicio, asesor_arl, obs_ppta, numorden";
            $fc= 1;
            $controlTxt="Nit Empresa";
            $idArl = "1";
        }elseif ($arl=='sura'){
            $nField =33;
            $fieldRead = array(0,1,6,17,18,31);
            $fieldInsert = "numorden, nombre_empresa, num_horas, f_inicio, f_termina, obs_ppta";
           $fc= 3;
            $controlTxt="ORDEN";
            $idArl = "7";

        }

        if ($archivo != ""){
            $delete="";
            $delete = "truncate `tb_ordenservicio_tmp`";
            $result=mysql_query($delete);


            $file = fopen($archivo, "r");
           $cfila = 1;
            $chkTxt = true;
            while (($data = fgetcsv($file, 5000, ";")) !== FALSE) {

                $num = count($data);
                if($num!=$nField){
                    $chkTxt = false;
                }
                $query = "INSERT INTO `tb_ordenservicio_tmp`(id_ordenservicio, ".$fieldInsert.", tiporden_id, arp_id, tipo_valor,
                            tipo_personal, formapago, fechaspago, sistema, fecha_elabora) values(NULL";
                if($cfila>=$fc) {
                    for ($c = 0; $c < $num; $c++) {
                        //Valida si el formato corresponda a la ARL
                        if($cfila==$fc){
                            $chkFile  = strpos($data[0],$controlTxt);
                            if($chkFile!==false) {
                                $chkTxt = true;
                            }
                            else{
                                $chkTxt = false;
                            }
                        }
                        //Se extrae sólo el campo que esté configurado en el aray
                        if(in_array($c,$fieldRead)) {
                            if (strlen($data[$c]) > 0) {
                                if($c==39&&$arl=='bolivar'){
                                   $dataField = ereg_replace("[^0-9]", "", $data[$c]);
                                }elseif(($c==17||$c==18)&&$arl=='sura'){
                                    $cnvDate = explode("/",$data[$c]);
                                    $dataField =  $cnvDate[2].'-'.$cnvDate[1].'-'.$cnvDate[0];
                                    $dataField= strtotime($dataField);
                                    $dataField=date('Y-m-d',$dataField);
                                }elseif($c==31 && $arl=='sura'){
                                    $dataField = "Contrato: ".$data[2].'\n';
                                    $dataField .= "Tarea: ".$data[3].'\n';
                                    $dataField .= "Producto: ".$data[4].'\n';
                                    $dataField .= "Observación: ".$data[$c];
                                }
                                else{
                                    $dataField = $data[$c];
                                }
                                $query = $query . ",'" . $dataField . "'";
                            } else {
                                $query = $query . ",''";
                            }
                        }
                    }

                    $query = $query.", '2', '".$idArl."', '1', '2', 'Las pactadas con la ARL', 'Las pactadas con la ARL', '18',NOW());";
                    mysql_query(($query))or die(mysql_error());

                }

                if($chkTxt==false){
                    $resultLoad = "error:Formato el formato cargado es inválido";
                    break;
                }


                $cfila = $cfila+1;
            }
            //Elimina primer registro de los titulos
            $query = "delete from `tb_ordenservicio_tmp` where id_ordenservicio=1";
            mysql_query($query);



            //Cambiamosdatos temporales
            $this->cambiaDatosTemporales($arl);
            if($chkTxt==true){
                $resultLoad = "success:Carga correcta";
            }
            return $resultLoad;
        }
    }

    function cambiaDatosTemporales($arl){

        //En caso que la arl sea bolivar
        if($arl=='bolivar') {
            //Actualizar servicio de Asesoria
            $query = "update tb_ordenservicio_tmp set tiposervicio1_id=1 where servicio='A'";
            mysql_query($query);

            //Actualizar servicio de Capacitacion
            $query = "update tb_ordenservicio_tmp set tiposervicio2_id=2 where servicio='C'";
            mysql_query($query);

            //Actualizar servicio de Asistencia tecnica
            $query = "update tb_ordenservicio_tmp set tiposervicio4_id=4 where servicio='T'";
            mysql_query($query);

            //Actualizar servicio de Servicio Especializado
            $query = "update tb_ordenservicio_tmp set tiposervicio5_id=5 where servicio='E'";
            mysql_query($query);

            //traer el id de los clientes
            $query = "update tb_ordenservicio_tmp AS o, tb_cliente AS c  set o.cliente_id=c.id_cliente where c.nit=o.nit_empresa";
            mysql_query($query)or die(mysql_error());

            //Insertamos los clientes que no está registrados
            $query = "INSERT INTO tb_cliente (nit, nom_cliente, auto_create) SELECT nit_empresa, nombre_empresa, '1' FROM `tb_ordenservicio_tmp` WHERE cliente_id IS NULL GROUP BY nit_empresa";
            mysql_query($query)or die(mysql_error());

            //Actualizamos la tabla temporal con los nuevo clientes creados
            $query = "update tb_ordenservicio_tmp AS o, tb_cliente AS c  set o.cliente_id=c.id_cliente where c.nit=o.nit_empresa AND o.cliente_id IS NULL";
            mysql_query($query)or die(mysql_error());
        }

        //En caso que la arl sea sura
        elseif($arl=='sura'){

            //traer el id de los clientes
            $query = "update tb_ordenservicio_tmp AS o, tb_cliente AS c  set o.cliente_id=c.id_cliente where c.nom_cliente = o.nombre_empresa";
            mysql_query($query)or die(mysql_error());

            //traer el id de los clientes temporales
            $query = "update tb_ordenservicio_tmp AS o, tb_cliente_tmp AS c  set o.cliente_id_tmp=c.id_cliente where c.nom_cliente=o.nombre_empresa AND o.cliente_id IS NULL";
            mysql_query($query)or die(mysql_error());

            //Insertamos los clientes que no está registrados en la tabla de clientes temporales
            $query = "INSERT INTO tb_cliente_tmp (nit, nom_cliente) SELECT NULL, nombre_empresa FROM `tb_ordenservicio_tmp` WHERE cliente_id IS NULL GROUP BY nombre_empresa";
            mysql_query($query)or die(mysql_error());

            //Actualizamos la tabla temporal con los nuevo clientes creados en la tabla de clientes temporales
            $query = "update tb_ordenservicio_tmp AS o, tb_cliente_tmp AS c  set o.cliente_id_tmp=c.id_cliente where c.nom_cliente=o.nombre_empresa AND o.cliente_id IS NULL";
            mysql_query($query)or die(mysql_error());
        }

        return  true;
    }

    function obtenerOrdenTemporal(){

        $sql= "SELECT * FROM tb_ordenservicio_tmp";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }

        return $rowAll;
    }

    function  checkOrdenSura($numero_orden){
        $sql= "SELECT * FROM tb_ordenservicio WHERE numorden ='".$numero_orden."'";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }

    function  checkOrdenBolivar($numero_orden, $secuencia, $cronograma){
        $sql= "SELECT * FROM tb_ordenservicio WHERE numorden ='".$numero_orden."' AND secuencia='".$secuencia."' AND cronograma = '".$cronograma."'";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }

    function migrarOrden($id_ordenservicio){
        $query = "INSERT INTO tb_ordenservicio (numorden, tiporden_id, arp_id, cliente_id, cliente_id_tmp, fecha_elabora, secuencia, 
                        cronograma, tiposervicio1_id, tiposervicio2_id, tiposervicio3_id, tiposervicio4_id, tiposervicio5_id, 
                        sistema, f_inicio, f_termina, num_horas, tipo_valor, tipo_personal, formapago, fechaspago, obs_ppta, 
                        fechacreate, asesor_arl, tema_bolivar, sipab_bolivar, estado,servicio) 
                  SELECT numorden, tiporden_id, arp_id, cliente_id, cliente_id_tmp, fecha_elabora, secuencia, 
                        cronograma, tiposervicio1_id, tiposervicio2_id, tiposervicio3_id, tiposervicio4_id, tiposervicio5_id, 
                        sistema, f_inicio, f_termina, num_horas, tipo_valor, tipo_personal, formapago, fechaspago, obs_ppta, 
                        NOW(), asesor_arl, tema_bolivar, sipab_bolivar, '1',servicio FROM `tb_ordenservicio_tmp` WHERE id_ordenservicio = '".$id_ordenservicio."'";
        mysql_query($query)or die(mysql_error());

    }

    function getClientNew(){
        $sql= "SELECT * FROM tb_cliente WHERE auto_create ='1'";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }

    function getClientRev(){
        $sql= "SELECT * FROM tb_cliente_tmp";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }

    function getClientById($id){
        $sql= "SELECT * FROM tb_cliente_tmp WHERE id_cliente='".$id."'";
        $query=mysql_query($sql);
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }

    function checkNit($nit){
        $sql= "SELECT * FROM tb_cliente WHERE nit='".$nit."'";
        $query=mysql_query($sql)or die(mysql_error());
        while ($row=mysql_fetch_object($query)){
            $rowAll[] = $row;
        }
        return $rowAll;
    }
    function insertaCliente($id,$form){
        //Insertamos el cliente en la tabla principal
        $sql="INSERT INTO tb_cliente SET 
                 nom_cliente ='".$form['nombrecli']."', 
                 nit ='".$form['nitcli']."', 
                 direccion ='".$form['dircli']."', 
                 telefono ='".$form['telcli']."', 
                 actividade ='".$form['actcli']."', 
                 ciudad_id ='".$form['sel_ciudad']."', 
                 observa ='".$form['obscli']."'                 
                 ";
        $query = mysql_query($sql)or die(mysql_error());
        $idCliente = mysql_insert_id();

        //cambiamos la id del cliente en todas las ordenes con id de cloiente temporal
        $updateOrden = "UPDATE tb_ordenservicio SET cliente_id='".$idCliente."', cliente_id_tmp='' where cliente_id_tmp='".$id."'";
        mysql_query($updateOrden)or die(mysql_error());

        //Eliminamos el cliente temporal
        $deleteOrder = "DELETE FROM tb_cliente_tmp where id_cliente='".$id."'";
        mysql_query($deleteOrder)or die(mysql_error());

    }

}

?>