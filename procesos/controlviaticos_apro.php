<?php
include_once ("../include/conexion.php");
include_once("../include/loginpru.php");
include_once("../include/proceso.php");

$objProceso = new Proceso();
$id_tipo = $id_t;
$id_resp = $id_r;
$objConexion = new Conectar();
//$objMenu = new Menu();
//$objProcesos= new Procesos();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<link rel="stylesheet" href="../css/jquery-ui-1.css" type="text/css">
<script src="../js/jquery.js"></script>
<script src="../js/jquery-ui.js"></script>
<script src="../js/combobox_codigo.js"></script>
<script>

$(document).ready(function() {
    $("#tb_usuario").combobox();
    $("#tb_usuario").change(function(){
      this.form.submit();
    });
    $("#tb_mesano").combobox();
    $("#tb_mesano").change(function(){
      this.form.submit();
    });
    $("#tb_arp").combobox();
    $("#tb_arp").change(function(){
      this.form.submit();
    });
});
function insertprefactura(num_prefactura, id_asignado){
    //if(num_fac > 0){    
    $.ajax({
              data:  {cruce_prefac:num_prefactura, id_asig:id_asignado, proceso:3},
              url:   'act_facs_seq.php',
              type:  'post',
        beforeSend: function () {
                          $("#no_asignacion"+id_asignado).html("Espere por favor...");
                  },

              success:  function (response) {
              //$("#no_asignacion"+id_asignado).html(response);
              $("#no_asignacion"+id_asignado).html("Registro actualizado");
          }
          });
    //}
  }

function valida_envia(){ 

   	//valido el nombre 
	 	if (document.fvalida.tf_fechent.value.length==0)
		{ 
      	 alert("Tiene que escribir la fecha de presentación de la propuesta") 
      	 document.fvalida.tf_fechent.focus() 
      	 return 0; 
	   	} 

 if (document.fvalida.sel_cliente.value==0)
		{ 
      	 alert("Tiene que seleccionar el Cliente") 
      	 document.fvalida.sel_cliente.focus() 
      	 return 0; 
	   	} 

	document.fvalida.submit();

}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administracion Orden de Servicio</title>
<link href="../css/reex.css" rel="stylesheet" type="text/css" />
<!--<script language="JavaScript" type="text/javascript" src="ajax_admactor.js"></script>-->
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<style type="text/css">

td img {display: block;}td img {display: block;}
</style>
</head>



<body onload="MM_preloadImages('../images/5-actividades_r2_c2_f2.jpg','../images/5-actividades_r2_c4_f2.jpg','../images/5-actividades_r2_c6_f2.jpg','../images/5-actividades_r4_c2_f2.jpg','../images/5-actividades_r4_c4_f2.jpg','../images/5-actividades_r4_c6_f2.jpg')"><table width="85%" border="0" align="center">
<tr>
<td><?php 
$tipo_menu = "Actividades";
include_once ("../include/menu_top.php");?></td>
</tr>
  <tr>

    <td><form name="fvalida" id="fvalida" action="" method="post" onSubmit="javascript:return valida_envia();" enctype="multipart/form-data">

      <table width="100%" border="0">

        <tr>          
          <td width="17971%" colspan="3" class="menutitulo">Viaticos Aprobados</td>        
        </tr>
        <tr>
              <td colspan="3" align="center" class="titulotablacampo2"><div id="onchange"><div class="ui-widget"><?php 
		if (($id_t==1)||($id_t==3))
		{
		echo "<select  name='tb_usuario' id='tb_usuario'  class='ingresoTabla' onchange='this.form.submit()' onChange='cargaContenido(this.id)'><option value='0'>-Seleccione Profesional-</option>";
  	echo $objProceso->SelectbusquedaT("tb_usuario",0, 2);
    echo "</select>";
		}
		  echo "<select  name='tb_mesano' id='tb_mesano'  class='ingresoTabla' onchange='this.form.submit()' onChange='cargaContenido(this.id)'><option value='0'>-Seleccione Fecha-</option>";

	 	  echo $objProceso->SelectbusquedaT("tb_mesano",1, 2);

		  echo "</select>";
      echo "<select  name='tb_arp' id='tb_arp'  class='ingresoTabla' onchange='this.form.submit()' onChange='cargaContenido(this.id)'><option value='20'>-Seleccione ARL-</option>";

      echo $objProceso->SelectbusquedaT("tb_arp",'id_arp', 'nom_arp');

      echo "</select>";  
		    ?></div></div></td>
            </tr>
        </table>

    </form></td>

  </tr>

  <tr>

    <td><div id="resultado"><?php include('viaticos_aprosel.php');?>

    </div>

    </td>

  </tr>
<tr>

    <td class="ok"><a href="controlviactivos_apro_imp.php?tb_mesano=<?=$_REQUEST['tb_mesano']; ?>&tb_usuario=<?=$_REQUEST['tb_usuario'];?>" target="_new">Ver Impresión Preliminar de Viaticos para Pre-Factura</a></td>

  </tr>    

</table>
</body>
</html>