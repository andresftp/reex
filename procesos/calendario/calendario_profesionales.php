<?php

include_once("../../include/conexion.php");
include_once("../../include/loginpru.php");
include_once("../../include/proceso.php");

$objProceso = new Proceso();

require_once('bdd.php'); 





//se recibe el id_usuario profesional y se carga su calendario correspondiente
$id_usuario_cal = $_SESSION['iduser'];




$sql = "SELECT id, title, start, end, color FROM events WHERE id_usuario =".$id_usuario_cal;

$req = $bdd->prepare($sql);
$req->execute();

$events = $req->fetchAll();

?>

<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Reex - Calendario Profesionales</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	
	<!-- FullCalendar -->
	<link href='css/fullcalendar.css' rel='stylesheet' />


    <!-- Custom CSS -->
    <style>
    body {
       
        
    }
	#calendar {
		max-width: 800px;
	}
	.col-centered{
		float: none;
		margin: 0 auto;
	}
    </style>



</head>

<body>

    

    <!-- Page Content -->
    <div class="container" style="">
        <div class="row">
            <div class="col-lg-12 text-center">
                             
                             <h3 style="color: #34495e">Calendario de Actividades</h3>
                             <hr>
                <div id="calendar" class="col-centered">
                </div>
            </div>
			<hr>
        </div>
        <!-- /.row -->
		
		<!-- Modal -->
		<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="addEvent.php">
			<input type="hidden" id="id_usuario_cal" name="id_usuario_cal" value="<?php echo $id_usuario_cal;?>">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Agregar Evento</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Titulo</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
					</div>
				  </div>
				  <div class="form-group" style="display: none;">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
									 						
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						
						  
						</select>
					</div>
				  </div>
				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Fecha</label>
					<div class="col-sm-10">
					  <input type="text" name="start" class="form-control" id="start" readonly>
					</div>
				  </div>

				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Hora Inicial</label>
					<div class="col-sm-10">
					  

						 <select name="color" class="form-control" id="color">
						
<option value="00:00">00:00 h.</option>
<option value="00:15">00:15 h.</option>
<option value="00:30">00:30 h.</option>
<option value="00:45">00:45 h.</option>
<option value="01:00">01:00 h.</option>
<option value="01:15">01:15 h.</option>
<option value="01:30">01:30 h.</option>
<option value="01:45">01:45 h.</option>
<option value="02:00">02:00 h.</option>
<option value="02:15">02:15 h.</option>
<option value="02:30">02:30 h.</option>
<option value="02:45">02:45 h.</option>
<option value="03:00">03:00 h.</option>
<option value="03:15">03:15 h.</option>
<option value="03:30">03:30 h.</option>
<option value="03:45">03:45 h.</option>
<option value="04:00">04:00 h.</option>
<option value="04:15">04:15 h.</option>
<option value="04:30">04:30 h.</option>
<option value="04:45">04:45 h.</option>
<option value="05:00">05:00 h.</option>
<option value="05:15">05:15 h.</option>
<option value="05:30">05:30 h.</option>
<option value="05:45">05:45 h.</option>
<option value="06:00">06:00 h.</option>
<option value="06:15">06:15 h.</option>
<option value="06:30">06:30 h.</option>
<option value="06:45">06:45 h.</option>
<option value="07:00">07:00 h.</option>
<option value="07:15">07:15 h.</option>
<option value="07:30">07:30 h.</option>
<option value="07:45">07:45 h.</option>
<option value="08:00">08:00 h.</option>
<option value="08:15">08:15 h.</option>
<option value="08:30">08:30 h.</option>
<option value="08:45">08:45 h.</option>
<option value="09:00">09:00 h.</option>
<option value="09:15">09:15 h.</option>
<option value="09:30">09:30 h.</option>
<option value="09:45">09:45 h.</option>
<option value="10:00">10:00 h.</option>
<option value="10:15">10:15 h.</option>
<option value="10:30">10:30 h.</option>
<option value="10:45">10:45 h.</option>
<option value="11:00">11:00 h.</option>
<option value="11:15">11:15 h.</option>
<option value="11:30">11:30 h.</option>
<option value="11:45">11:45 h.</option>
<option value="12:00">12:00 h.</option>
<option value="12:15">12:15 h.</option>
<option value="12:30">12:30 h.</option>
<option value="12:45">12:45 h.</option>
<option value="13:00">13:00 h.</option>
<option value="13:15">13:15 h.</option>
<option value="13:30">13:30 h.</option>
<option value="13:45">13:45 h.</option>
<option value="14:00">14:00 h.</option>
<option value="14:15">14:15 h.</option>
<option value="14:30">14:30 h.</option>
<option value="14:45">14:45 h.</option>
<option value="15:00">15:00 h.</option>
<option value="15:15">15:15 h.</option>
<option value="15:30">15:30 h.</option>
<option value="15:45">15:45 h.</option>
<option value="16:00">16:00 h.</option>
<option value="16:15">16:15 h.</option>
<option value="16:30">16:30 h.</option>
<option value="16:45">16:45 h.</option>
<option value="17:00">17:00 h.</option>
<option value="17:15">17:15 h.</option>
<option value="17:30">17:30 h.</option>
<option value="17:45">17:45 h.</option>
<option value="18:00">18:00 h.</option>
<option value="18:15">18:15 h.</option>
<option value="18:30">18:30 h.</option>
<option value="18:45">18:45 h.</option>
<option value="19:00">19:00 h.</option>
<option value="19:15">19:15 h.</option>
<option value="19:30">19:30 h.</option>
<option value="19:45">19:45 h.</option>
<option value="20:00">20:00 h.</option>
<option value="20:15">20:15 h.</option>
<option value="20:30">20:30 h.</option>
<option value="20:45">20:45 h.</option>
<option value="21:00">21:00 h.</option>
<option value="21:15">21:15 h.</option>
<option value="21:30">21:30 h.</option>
<option value="21:45">21:45 h.</option>
<option value="22:00">22:00 h.</option>
<option value="22:15">22:15 h.</option>
<option value="22:30">22:30 h.</option>
<option value="22:45">22:45 h.</option>
<option value="23:00">23:00 h.</option>
<option value="23:15">23:15 h.</option>
<option value="23:30">23:30 h.</option>
<option value="23:45">23:45 h.</option>

						
						  
						</select>	


					</div>
				  </div>

				  <div class="form-group">
					<label for="start" class="col-sm-2 control-label">Hora Final</label>
					<div class="col-sm-10">
					 
 <select name="color" class="form-control" id="color">

<option value="00:00">00:00 h.</option>
<option value="00:15">00:15 h.</option>
<option value="00:30">00:30 h.</option>
<option value="00:45">00:45 h.</option>
<option value="01:00">01:00 h.</option>
<option value="01:15">01:15 h.</option>
<option value="01:30">01:30 h.</option>
<option value="01:45">01:45 h.</option>
<option value="02:00">02:00 h.</option>
<option value="02:15">02:15 h.</option>
<option value="02:30">02:30 h.</option>
<option value="02:45">02:45 h.</option>
<option value="03:00">03:00 h.</option>
<option value="03:15">03:15 h.</option>
<option value="03:30">03:30 h.</option>
<option value="03:45">03:45 h.</option>
<option value="04:00">04:00 h.</option>
<option value="04:15">04:15 h.</option>
<option value="04:30">04:30 h.</option>
<option value="04:45">04:45 h.</option>
<option value="05:00">05:00 h.</option>
<option value="05:15">05:15 h.</option>
<option value="05:30">05:30 h.</option>
<option value="05:45">05:45 h.</option>
<option value="06:00">06:00 h.</option>
<option value="06:15">06:15 h.</option>
<option value="06:30">06:30 h.</option>
<option value="06:45">06:45 h.</option>
<option value="07:00">07:00 h.</option>
<option value="07:15">07:15 h.</option>
<option value="07:30">07:30 h.</option>
<option value="07:45">07:45 h.</option>
<option value="08:00">08:00 h.</option>
<option value="08:15">08:15 h.</option>
<option value="08:30">08:30 h.</option>
<option value="08:45">08:45 h.</option>
<option value="09:00">09:00 h.</option>
<option value="09:15">09:15 h.</option>
<option value="09:30">09:30 h.</option>
<option value="09:45">09:45 h.</option>
<option value="10:00">10:00 h.</option>
<option value="10:15">10:15 h.</option>
<option value="10:30">10:30 h.</option>
<option value="10:45">10:45 h.</option>
<option value="11:00">11:00 h.</option>
<option value="11:15">11:15 h.</option>
<option value="11:30">11:30 h.</option>
<option value="11:45">11:45 h.</option>
<option value="12:00">12:00 h.</option>
<option value="12:15">12:15 h.</option>
<option value="12:30">12:30 h.</option>
<option value="12:45">12:45 h.</option>
<option value="13:00">13:00 h.</option>
<option value="13:15">13:15 h.</option>
<option value="13:30">13:30 h.</option>
<option value="13:45">13:45 h.</option>
<option value="14:00">14:00 h.</option>
<option value="14:15">14:15 h.</option>
<option value="14:30">14:30 h.</option>
<option value="14:45">14:45 h.</option>
<option value="15:00">15:00 h.</option>
<option value="15:15">15:15 h.</option>
<option value="15:30">15:30 h.</option>
<option value="15:45">15:45 h.</option>
<option value="16:00">16:00 h.</option>
<option value="16:15">16:15 h.</option>
<option value="16:30">16:30 h.</option>
<option value="16:45">16:45 h.</option>
<option value="17:00">17:00 h.</option>
<option value="17:15">17:15 h.</option>
<option value="17:30">17:30 h.</option>
<option value="17:45">17:45 h.</option>
<option value="18:00">18:00 h.</option>
<option value="18:15">18:15 h.</option>
<option value="18:30">18:30 h.</option>
<option value="18:45">18:45 h.</option>
<option value="19:00">19:00 h.</option>
<option value="19:15">19:15 h.</option>
<option value="19:30">19:30 h.</option>
<option value="19:45">19:45 h.</option>
<option value="20:00">20:00 h.</option>
<option value="20:15">20:15 h.</option>
<option value="20:30">20:30 h.</option>
<option value="20:45">20:45 h.</option>
<option value="21:00">21:00 h.</option>
<option value="21:15">21:15 h.</option>
<option value="21:30">21:30 h.</option>
<option value="21:45">21:45 h.</option>
<option value="22:00">22:00 h.</option>
<option value="22:15">22:15 h.</option>
<option value="22:30">22:30 h.</option>
<option value="22:45">22:45 h.</option>
<option value="23:00">23:00 h.</option>
<option value="23:15">23:15 h.</option>
<option value="23:30">23:30 h.</option>
<option value="23:45">23:45 h.</option>
</select>


					</div>
				  </div>


				
					  <input type="hidden" name="end" class="form-control" id="end" readonly>
				
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>
		
		
		
		<!-- Modal -->
		<div class="modal fade" id="ModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			<form class="form-horizontal" method="POST" action="editEventTitle.php">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Modificar Evento</h4>
			  </div>
			  <div class="modal-body">
				
				  <div class="form-group">
					<label for="title" class="col-sm-2 control-label">Titulo</label>
					<div class="col-sm-10">
					  <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
					</div>
				  </div>
				  <div class="form-group">
					<label for="color" class="col-sm-2 control-label">Color</label>
					<div class="col-sm-10">
					  <select name="color" class="form-control" id="color">
						  <option value="">Seleccionar</option>
						  <option style="color:#0071c5;" value="#0071c5">&#9724; Azul oscuro</option>
						  <option style="color:#40E0D0;" value="#40E0D0">&#9724; Turquesa</option>
						  <option style="color:#008000;" value="#008000">&#9724; Verde</option>						  
						  <option style="color:#FFD700;" value="#FFD700">&#9724; Amarillo</option>
						  <option style="color:#FF8C00;" value="#FF8C00">&#9724; Naranja</option>
						  <option style="color:#FF0000;" value="#FF0000">&#9724; Rojo</option>
						  <option style="color:#000;" value="#000">&#9724; Negro</option>
						  
						</select>
					</div>
				  </div>
				    <div class="form-group"> 
						<div class="col-sm-offset-2 col-sm-10">
						  <div class="checkbox">
							<label class="text-danger"><input type="checkbox"  name="delete"> Eliminar Evento</label>
						  </div>
						</div>
					</div>
				  
				  <input type="hidden" name="id" class="form-control" id="id">
				
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			  </div>
			</form>
			</div>
		  </div>
		</div>

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
	<!-- FullCalendar -->
	<script src='js/moment.min.js'></script>
	<script src='js/fullcalendar/fullcalendar.min.js'></script>
	<script src='js/fullcalendar/fullcalendar.js'></script>
	<script src='js/fullcalendar/locale/es.js'></script>
	
	
	<script>

	$(document).ready(function() {

		var date = new Date();
       var yyyy = date.getFullYear().toString();
       var mm = (date.getMonth()+1).toString().length == 1 ? "0"+(date.getMonth()+1).toString() : (date.getMonth()+1).toString();
       var dd  = (date.getDate()).toString().length == 1 ? "0"+(date.getDate()).toString() : (date.getDate()).toString();
		
		$('#calendar').fullCalendar({
			header: {
				 language: 'es',
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay',

			},
			defaultDate: yyyy+"-"+mm+"-"+dd,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			selectable: true,
			selectHelper: true,
			select: function(start, end) {
				
				$('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD'));
				$('#ModalAdd #end').val(moment(end).format('YYYY-MM-DD'));
				$('#ModalAdd').modal('show');
			},
			eventRender: function(event, element) {
				element.bind('dblclick', function() {
					$('#ModalEdit #id').val(event.id);
					$('#ModalEdit #title').val(event.title);
					$('#ModalEdit #color').val(event.color);
					$('#ModalEdit').modal('show');
				});
			},
			eventDrop: function(event, delta, revertFunc) { // si changement de position

				edit(event);

			},
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

				edit(event);

			},
			events: [
			<?php foreach($events as $event): 
			
				$start = explode(" ", $event['start']);
				$end = explode(" ", $event['end']);
				if($start[1] == '00:00:00'){
					$start = $start[0];
				}else{
					$start = $event['start'];
				}
				if($end[1] == '00:00:00'){
					$end = $end[0];
				}else{
					$end = $event['end'];
				}
			?>
				{
					id: '<?php echo $event['id']; ?>',
					title: '<?php echo $event['title']; ?>',
					start: '<?php echo $start; ?>',
					end: '<?php echo $end; ?>',
					color: '<?php echo $event['color']; ?>',
				},
			<?php endforeach; ?>
			]
		});
		
		function edit(event){
			start = event.start.format('YYYY-MM-DD HH:mm:ss');
			if(event.end){
				end = event.end.format('YYYY-MM-DD HH:mm:ss');
			}else{
				end = start;
			}
			
			id =  event.id;
			
			Event = [];
			Event[0] = id;
			Event[1] = start;
			Event[2] = end;
			
			$.ajax({
			 url: 'editEventDate.php',
			 type: "POST",
			 data: {Event:Event},
			 success: function(rep) {
					if(rep == 'OK'){
						alert('Evento se ha guardado correctamente');
					}else{
						alert('No se pudo guardar. Inténtalo de nuevo.'); 
					}
				}
			});
		}
		
	});

</script>

</body>

</html>
