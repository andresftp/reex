<?php
include_once("../include/conexion.php");
include_once("../include/loginpru.php");
//include_once("../include/menu.php");
//include_once("../../include/procesos.php");

$id_tipo = $id_t;
$id_resp = $id_r;
$objConexion = new Conectar();
//$objMenu = new Menu();
//$objProcesos= new Procesos();

$consulta_sql="SELECT * FROM tb_cartelera WHERE estado=1";
$sql=mysql_query($consulta_sql);
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Importante</title>
    <link href="../css/reex.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../js/jquery1-9.js"></script>
</head>
<body>
<table width="75%" border="0" align="center">
    <tr>
        <td align="center"><img src="../imagenes/logoreex.jpg" width="250" height="140" /></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td><table width="100%" border="0" align="center">
                <tr>
                    <td colspan="2" class="menutitulocapaC">PARA TENER EN CUENTA </td>
                </tr>
                <?php
                $num=1;
                while($row = mysql_fetch_array($sql)){
                    ?>
                    <tr>
                        <td width="10%" rowspan="2" align="center"><img src="../imagenes/imporvineta.jpg" width="40" height="40" /></td>
                        <td width="90%" class="error2"><?php echo $row['titulo']; ?></td>
                    </tr>
                    <tr>
                        <td class="encabezadoFormato2"><?php echo $row['descripcion']; ?></td>
                    </tr>
                    <?php $num=$num+1;
                } ?>
            </table>
        </td>

    </tr>
    <tr>
        <td>
            <br>
            <br>
            <br>
            <br>
            <table width="100%" border="0" align="center">
                <tr>
                    <td width="10%" rowspan="2" align="center"><img src="../imagenes/imporvineta.jpg" width="40" height="40" /></td>
                    <td width="90%" class="error2">ALERTA DE PENALIDADES</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table width="100%" border="1">
                            <tr>
                                <td class="titulotablacampo">#</td>
                                <td class="titulotablacampo">ARL</td>
                                <td class="titulotablacampo">Orden</td>
                                <td class="titulotablacampo">Consultor</td>
                                <td class="titulotablacampo">Servicio</td>
                                <td class="titulotablacampo">Reunión Inicial</td>
                                <td class="titulotablacampo">Días por Penalizar</td>
                            </tr>

                        <?php
                        $fecha_actual = date("Y-m-d");
                        //resto 1 día
                        $fecha_30 =  date("Y-m-d",strtotime($fecha_actual."- 31 days"));
                        $conusu =  (($id_t==1)||($id_t==3))?'':' AND u.id_usuario='.$id_r;

                        $sql_act = "SELECT *, horas_asg - sumahoras AS horas_restantes FROM ( 
                                                SELECT a.id_asignacion, o.numorden, c.nom_cliente, u.nom_usuario, a.horas_asg, a.viaticos_asg, a.observaciones, 
                                                    t.nom_tiposervicio, o.viaticos, a.fechacreate, a.fecha_reinicial, SUM(ac.cant_h) AS sumahoras, ac.asignacion_id, 
                                                    arp.nom_arp, o.secuencia, o.f_termina, a.fecha_realizacion 
                                                FROM tb_asignacion a LEFT JOIN tb_ordenservicio o ON (o.id_ordenservicio=a.ordenservicio_id) 
                                                LEFT JOIN tb_cliente c ON (c.id_cliente=o.cliente_id) 
                                                LEFT JOIN tb_usuario u ON (u.id=a.usuario_id) 
                                                LEFT JOIN tb_tiposervicio t ON (a.tiposervicio_id=t.id_tiposervicio) 
                                                LEFT JOIN tb_actividades ac ON (a.id_asignacion = ac.asignacion_id) 
                                                LEFT JOIN tb_arp arp ON (arp.id_arp=o.arp_id) 
                                                WHERE a.estado=1 AND a.fecha_reinicial > '" . $fecha_30 . "' ".$conusu."
                                                GROUP BY a.id_asignacion, o.numorden 
                                                ORDER BY a.fecha_reinicial DESC,c.nom_cliente
                                    ) AS tabla ";


                        $res = mysql_query($sql_act);
                        $ccc=0;
                        while($row = mysql_fetch_array($res)){
                            $shtr = 0;
                            $penaliza = 0;
                            $aprb =0;
                            if($row['fecha_realizacion']!='0000-00-00') {
                                //Penalizacion de horas
                                $queryDoc = "SELECT * FROM `tb_actividades` AS act 
                                LEFT JOIN `tb_controlact` AS ca ON ca.actividad_id = act.id_actividad
                            WHERE act.asignacion_id ='" . $row[0] . "'";
                                $resDoc = mysql_query($queryDoc);
                                $numresdoc = mysql_num_rows($resDoc);
                                if ($numresdoc == 0) {
                                    $penaliza = 1;
                                } else {
                                    $penaliza = 0;
                                    while ($rowpen = mysql_fetch_array($resDoc)) {
                                        if ($rowpen['aprobado_planea'] == 0 || $rowpen['aprobado_planea'] == 'null') {
                                            $penaliza++;
                                        }elseif ($rowpen['penalizacion']==1) {
                                            $penaliza++;
                                        }else{
                                            $aprb++;
                                        }
                                    }
                                }
                                $fecha = new DateTime($row['fecha_realizacion']);
                                $fecha->add(new DateInterval('P5D'));

                                $fechaven = date_create($fecha->format('Y-m-d'));
                                $fechaven = date_format($fechaven, 'Y-m-d');
                                $datetime1 = new DateTime(date('Y-m-d'));
                                $datetime2 = new DateTime($fechaven);
                                $interval = $datetime1->diff($datetime2);
                                $dias = $interval->format('%a');
                                if($dias==0){
                                    $sig = "Último día";
                                    $cr = ' style="color:#e50e13"';
                                }elseif(strtotime(date('Y-m-d')) > strtotime($fechaven)) {
                                    $sig = "Penalizado";
                                    $cr = ' style="color:#e50e13"';
                                } else {
                                    $sig = "-".$dias;
                                    $cr = ' style="color:#e50e13"';
                                }

                                $dp='';
                                if(strtotime(date('Y-m-d')) > strtotime($fechaven)&&$penaliza>0){
                                    $dp = '<label ' . $cr . '>' .  $sig . '</label>';
                                    $shtr = 1;
                                }elseif (strtotime(date('Y-m-d')) > strtotime($fechaven)&&$aprb>0){
                                    $dp = 'No se penaliza';
                                    $shtr = 0;
                                }elseif (strtotime(date('Y-m-d')) < strtotime($fechaven)&&$aprb>0){
                                    $dp = 'No se penaliza';
                                    $shtr = 0;
                                }elseif (strtotime(date('Y-m-d')) < strtotime($fechaven)&&$dias>5){
                                    $dp = 'A tiempo';
                                    $shtr = 1;
                                }elseif($aprb>0){
                                    $shtr = 1;
                                    $dp = 'No se penaliza';
                                }else{
                                    $shtr = 1;
                                    $dp = '<label ' . $cr . '>' .  $sig . '</label>';
                                }

                            }

                            if($shtr==1){
                                $ccc++;
                                echo '<tr>
                                        <td class="contenidoTabla2" align="center">'.$ccc.'</td>
                                        <td class="contenidoTabla2" align="center">'.$row['nom_arp'].'</td>                                       
                                        <td class="contenidoTabla2" align="center">'.$row[1]."-".$row[2].'</td>                                       
                                        <td class="contenidoTabla2" align="center">'.$row[3].'</td>                                       
                                        <td class="contenidoTabla2" align="center">'.$row[7].'</td>                                       
                                        <td class="contenidoTabla2" align="center">'.$row[10].'</td>                                       
                                        <td class="contenidoTabla2" align="center">'.$dp.'</td>                                       
                                  </tr>';
                            }
                        }
                        ?>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php if($id_resp==29820662 OR $id_resp==1032411154 ){ ?>
    <td><div><?php include('../include/listado_cons_sin_seg_soc.php' ); ?></div><td>
        <?php } ?>

</table>
</body>
</html>