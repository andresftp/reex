<?php
include_once ("../include/conexion.php");
include_once("../include/loginpru.php");
include_once("../include/proceso.php");

$id_tipo = $id_t;
$id_resp = $id_r;
$objConexion = new Conectar();
$objProceso = new Proceso();

$consulta_sql="SELECT id_ordenservicio, nom_cliente, numorden, estado, nom_tiporden, nom_arp, secuencia, cronograma, num_horas, USUARIO, id_asignacion, horas_asg, nom_usuario, SUMACT, DIFERENCIA, propuesta_id, viaticos FROM(
SELECT O.id_ordenservicio, C.nom_cliente, O.numorden, O.estado, T.nom_tiporden, A.nom_arp, O.secuencia, O.cronograma, O.f_inicio, O.f_termina, O.num_horas, O.valor, O.viaticos, O.formapago, O.fechaspago, C1.nom_cliente AS USUARIO, AG.id_asignacion, AG.horas_asg, U. nom_usuario, O.propuesta_id, SUM(AC.cant_h) SUMACT,(AG.horas_asg-SUM(AC.cant_h)) AS DIFERENCIA 
FROM tb_ordenservicio O 
INNER JOIN tb_cliente C ON (C.id_cliente=O.cliente_id)
LEFT OUTER JOIN tb_cliente C1 ON (C1.id_cliente=O.usuariocliente_id)    
INNER JOIN tb_arp A ON (A.id_arp=O.arp_id)
INNER JOIN tb_tiporden T ON (T.id_tiporden=O.tiporden_id) 
INNER JOIN tb_asignacion AG ON ( O.id_ordenservicio = AG.ordenservicio_id)
INNER JOIN tb_actividades AC ON (AG.id_asignacion=AC.asignacion_id)
INNER JOIN tb_usuario U ON ( U.id = AG.usuario_id ) 
WHERE O.estado=1 GROUP BY AG.id_asignacion ORDER BY C.nom_cliente) X
WHERE X.DIFERENCIA>0 ORDER BY X.DIFERENCIA DESC";
$sql=mysql_query($consulta_sql);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<script src="../SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Administracion Orden de Servicio</title>
<link href="../css/reex.css" rel="stylesheet" type="text/css" />
<script src="../Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
<!--<script language="JavaScript" type="text/javascript" src="ajax_admactor.js"></script>-->
<script src="../SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<link href="../SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="../SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<style type="text/css">

td img {display: block;}td img {display: block;}
</style>
<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
</head>
<body onload="MM_preloadImages('../images/3-planeacion_r2_c2_f2.jpg','../images/3-planeacion_r2_c4_f2.jpg')">
<table width="85%" border="0" align="center">
<tr>
<td><?php 
$tipo_menu = "Planeacion";
include_once ("../include/menu_top.php");?></td>
</tr>
  <tr>

    <td><form name="form" id="form" action="" method="get">

      <table width="100%" border="0">
        <tr>
          <td colspan="3" class="menutitulo">Faltantes de Horas Por Ejecutar</td>
        </tr>
        <tr>         
          <td colspan="3">
          <table border="0" width="100%" style="border:1px solid #FF0000; color:#3F658C;width:100%;">
            <tr>
              <td colspan="16" align="center" class="titulotablacampo2"><div id="onchange">
			  Fecha de Asignaci&oacute;n: <?
		  echo "<select  name='tb_mesano' id='tb_mesano'  class='ingresoTabla' onchange='this.form.submit()' onChange='cargaContenido(this.id)'><option value='0'>-Seleccione Fecha-</option>";

	 	  echo $objProceso->SelectbusquedaT("tb_mesano",1, 2);

		  echo "</select>"; 
		    ?></div></td>
            </tr>
<?php     			// if (($id_tipo<=2)||($id_tipo==5))
//				{		
				$rsDocumentos=$objProceso->getOrdenes_faltahoras($_REQUEST['tb_mesano']);
//				}
/*				if (($id_tipo==3)||($id_tipo==4))
				{		
				$rsDocumentos=$objProceso->getDocumento_allActR($_REQUEST['TB_ETAPA'],$proy1,$proy2,$proy3,$proy4);
				}*/
				$numr=mysql_num_rows($rsDocumentos);
				if ($_REQUEST['tb_mesano']>0) { $msj=" Cantidad de Registros del Mes ".$_REQUEST['tb_mesano']. " es ".$numr; } else {$msj="Cantidad de Registros Totales ".$numr; }
  ?>         
<tr>
	<td colspan="16"><div class="error" id="onchange"><?php echo $msj;?>
	</div></td>
	</tr>	
	<tr>				

   <tr>
              <td width="2%" align="center" class="titulotablacampo2">#</td>
              <td width="10%" align="center" class="titulotablacampo2">Cliente/Usuario</td>
              <td width="6%" align="center" class="titulotablacampo2"># Orden o P&oacute;liza</td>
              <td width="6%" align="center" class="titulotablacampo2">Tipo</td>
              <td width="6%" align="center" class="titulotablacampo2">ARL</td>
              <td width="6%" align="center" class="titulotablacampo2">Secuencia</td>
              <td width="6%" align="center" class="titulotablacampo2">Cronograma</td>
              <td width="5%" align="center" class="titulotablacampo2"># Horas</td>
              <td width="8%" align="center" class="titulotablacampo2">Consultor</td>
              <td width="9%" align="center" class="titulotablacampo2">Horas Asignadas</td>
              <td width="10%" align="center" class="titulotablacampo2">Horas Ejecutadas</td>
              <td width="9%" align="center" class="titulotablacampo2">Horas Faltantes</td>
              <td width="5%" align="center" class="titulotablacampo2">Opciones</td>
            </tr>
            <?php
$num=1;

while($row = mysql_fetch_array($rsDocumentos)){
//$archivolog="http://reex.rehabilitarexpress.com/archivos_propuesta/";
/*$servicios=NULL;
$nomservicio1=NULL;
$nomservicio2=NULL;
$nomservicio3=NULL;
$nomservicio4=NULL;
$nomservicio5=NULL;
$s1=$row[15];
if ($s1==1) {$nomservicio1="Asesoria";} 
$s2=$row[16];
if ($s2==2) {$nomservicio2="Auditoria";}
$s3=$row[17];
if ($s3==3) {$nomservicio3="Capacitacion";}
$s4=$row[18];
if ($s4==4) {$nomservicio4="Consultoria";}
$s5=$row[19];
if ($s5==5) {$nomservicio5="Asesoria Legal";}
$servicios=$nomservicio1.' '.$nomservicio2.' '.$nomservicio3.' '.$nomservicio4.' '.$nomservicio5;
*/
	echo " 		<td class='contenidoTabla3' align='center'>".$num."</td>";
/*	if ($row[20]>=1){ 
		$sqlusu="Select nom_cliente From tb_cliente Where id_cliente=$row[20]";
		$usu=mysql_query($sqlusu);
		$rowusu= mysql_fetch_array($usu);
	}*/	
	echo " 		<td class='contenidoTabla3'>".$row[1].'-'.$row[9]."</td>";
	echo " 		<td class='contenidoTabla3'>".$row[2]."</td>";
//	echo " 		<td class='contenidoTabla3'>".$servicios."</td>";
	echo " 		<td class='contenidoTabla3'>".$row[4]."</td>";
	echo " 		<td class='contenidoTabla3'>".$row[5]."</td>";
	echo " 		<td class='contenidoTabla3'>".$row[6]."</td>";
	echo " 		<td class='contenidoTabla3'>".$row[7]."</td>";
	echo " 		<td class='contenidoTabla3' align='center'>".$row[8]."</td>";	
	echo " 		<td class='contenidoTabla3'>".$row[12]."</td>";		
	echo " 		<td class='contenidoTabla3' align='center'>".$row[11]."</td>";		
	echo " 		<td class='contenidoTabla3' align='center'>".$row[13]."</td>";		
	echo " 		<td class='contenidoTabla3' align='center'>".$row[14]."</td>";		
//	echo " 		<td class='contenidoTabla3'>".$row[14]."</td>";							
?>
              <td align='center'>                
                <a href='ordenserviciocre.php?mod=mod&id=<?=$row[0]?>&idp=<?=$row[15]?>&usuario=<?=$row['id_cliente_aseg']?>'><img src='../imagenes/orden.jpg' width='20' height='20' border='0' target='_blank' title='Asignacion Consultores'/></a><a href='planeacioncontor.php?ido=<?=$row[0]?>&amp;vi=<?=$row[16]?>'><img src='../imagenes/consultores.jpg' width='20' height='20' border='0' target='_blank' title='Asignacion Consultores'/></a>
                <?php // if($objProcesos->actor($row['id_clasificacion']) == 0){ echo "<input type=\"checkbox\" name=\"seleccion[]\" value=\"".$row['id_clasificacion']."\">";} ?></td>
</tr>
<?php $servicios=NULL; $num=$num+1;
} ?>
</table>
</td>
</tr>
</table>
    </form></td>

  </tr>

<!--  <tr>

<td><div id="resultado"><?php include('ordenserviciocre_sel.php');?>

    </div>

    </td>

  </tr>-->

</table>
</body>
</html>