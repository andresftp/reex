//Iniciamos nuestra función jquery.
$(function(){
	$('#enviarHv').click(function(){
     var archivos = document.getElementById("hoja_vida"); 
     var tipo_archivo = 'Hoja_de_vida'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarCedula').click(function(){
     var archivos = document.getElementById("cedula"); 
     var tipo_archivo = 'Cedula'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarTarjetaPro').click(function(){
     var archivos = document.getElementById("tarjeta_profesional"); 
     var tipo_archivo = 'Tarjeta_profesional'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarCertifiLab').click(function(){
     var archivos = document.getElementById("certificaciones_laborales"); 
     var tipo_archivo = 'Certificaciones_laborales'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarCertificadoForm').click(function(){
     var archivos = document.getElementById("certificados_formacion"); 
     var tipo_archivo = 'Certificados_formacion'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarRut').click(function(){
     var archivos = document.getElementById("rut"); 
     var tipo_archivo = 'RUT'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarCertificacionBancaria').click(function(){
     var archivos = document.getElementById("certificacion_bancaria"); 
     var tipo_archivo = 'Certificacion_bancaria'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	$('#enviarExamenOcupacional').click(function(){
     var archivos = document.getElementById("examen_ocupacional"); 
     var tipo_archivo = 'Examen_ocupacional'; 
     SubirDocumentos(archivos,tipo_archivo);  //Capturamos el evento click sobre el boton con el id=enviar	y ejecutamos la función seleccionado.
	});
	
    $('.eliminar').click(function(){
        
        var id_archivo = $(this).attr('data');
	    var dataString = 'id_archivo='+id_archivo;
		swal({
			  title: "Esta seguro?",
			  text: "No se podra recuperar el archivo nuevamente, será eliminado permanentemente!",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Si, eliminarlo!",
			  cancelButtonText: "No, cancelar!",
			  closeOnConfirm: false,
			  closeOnCancel: false
			},
			function(isConfirm){
			  if (isConfirm) {
				 $.ajax({
					type: "POST",
					url: "../include/eliminar_archivos.php",
					data: dataString
					}).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
					MensajeEliminar(msg,id_archivo)
				});
			    swal("Eliminado!", "El archivo ha sido eliminado", "success");
			  } else {
				    swal("Cancelado", "El archivo se encuentra seguro", "error");
			  }
			});
       
	});

});

function SubirDocumentos(archivos,tipo_archivo){	
		var archivos = archivos;//Creamos un objeto con el elemento que contiene los archivos: el campo input file, que tiene el id = 'archivos'
		var tipo_archivo = tipo_archivo;
		var asesor = $('#asesor').val();
		var archivo = archivos.files; //Obtenemos los archivos seleccionados en el imput
		//Creamos una instancia del Objeto FormData.
		var archivos = new FormData();
		
		/* Como son multiples archivos creamos un ciclo for que recorra la el arreglo de los archivos seleccionados en el input
		Este y añadimos cada elemento al formulario FormData en forma de arreglo, utilizando la variable i (autoincremental) como 
		indice para cada archivo, si no hacemos esto, los valores del arreglo se sobre escriben*/
		for(i=0; i<archivo.length; i++){
		archivos.append('archivo'+i,archivo[i]); //Añadimos cada archivo a el arreglo con un indice direfente
		}
		/*Ejecutamos la función ajax de jQuery*/		
		$.ajax({
			url:'../include/subir_archivos.php?tipo_archivo='+tipo_archivo+'&asesor='+asesor, //Url a donde la enviaremos
			type:'POST', //Metodo que usaremos
			contentType:false, //Debe estar en false para que pase el objeto sin procesar
			data:archivos, //Le pasamos el objeto que creamos con los archivos
			processData:false, //Debe estar en false para que JQuery no procese los datos a enviar
			cache:false //Para que el formulario no guarde cache
		}).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
			MensajeFinal(msg)
			consultaArchivos(asesor,tipo_archivo)			
		});
	}

function MensajeFinal(msg){
	$('.mensaje').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('.mensaje').show('slow');//Mostramos el div.
}
function MostrarDivHV(msg){
	$('#listadoHV').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoHV').show('slow');//Mostramos el div.
	}
function MostrarDivCedula(msg){
	$('#listadoCedula').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoCedula').show('slow');//Mostramos el div.
}
function MostrarDivTarjePro(msg){
	$('#listadoTarjePro').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoTarjePro').show('slow');//Mostramos el div.
}
function MostrarDivCertLab(msg){
	$('#listadoCertLab').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoCertLab').show('slow');//Mostramos el div.
}
function MostrarDivCertForm(msg){
	$('#listadoCertForm').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoCertForm').show('slow');//Mostramos el div.
}
function MostrarDivRut(msg){
	$('#listadoRut').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoRut').show('slow');//Mostramos el div.
}
function MostrarDivCertBanc(msg){
	$('#listadoCertBanc').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoCertBanc').show('slow');//Mostramos el div.
}
function MostrarDivExOcupa(msg){
	$('#listadoExOcupa').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('#listadoExOcupa').show('slow');//Mostramos el div.
}
function MensajeEliminar(msg,id_archivo){
	$('.mensaje').html(msg);//A el div con la clase msg, le insertamos el mensaje en formato  thml
	$('.mensaje').show('slow');//Mostramos el div.
	$('#archivo'+id_archivo).remove();
}
function consultaArchivos(asesor,tipo_archivo){
	/*Ejecutamos la función ajax de jQuery*/	
	
	 var dataString = 'tipo_archivo='+tipo_archivo+'&asesor='+asesor;	
		$.ajax({
			url:'../include/consultar_archivos.php', //Url a donde la enviaremos
			type:'POST', //Metodo que usaremos
			data:dataString
		}).done(function(msg){//Escuchamos la respuesta y capturamos el mensaje msg
			switch(tipo_archivo) {
				case 'Hoja_de_vida':
					MostrarDivHV(msg)
					break;
				case 'Cedula':
					MostrarDivCedula(msg)
					break;
				case 'Tarjeta_profesional':
					MostrarDivTarjePro(msg)
					break;
				case 'Certificaciones_laborales':
					MostrarDivCertLab(msg)
					break;
				case 'Certificados_formacion':
					MostrarDivCertForm(msg)
					break;
				case 'RUT':
					MostrarDivRut(msg)
					break;
				case 'Certificacion_bancaria':
					MostrarDivCertBanc(msg)
					break;	
				case 'Examen_ocupacional':
					MostrarDivExOcupa(msg)
					break;
				}
 
		});
	}