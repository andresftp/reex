function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false;
	try
	{
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			// Creacion del objet AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(E)
		{
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') xmlhttp=new XMLHttpRequest();
		}
	}
	return xmlhttp; 
}

// Declaro los selects que componen el documento HTML. Su atributo ID debe figurar aqui.
var listadoSelects=new Array();
listadoSelects[0]="select1";
listadoSelects[1]="select2";
listadoSelects[2]="select3";
listadoSelects[3]="select4";
listadoSelects[4]="select5";


function buscarEnArray(array, dato)
{
	// Retorna el indice de la posicion donde se encuentra el elemento en el array o null si no se encuentra
	var x=0;
	while(array[x])
	{
		if(array[x]==dato) return x;
		x++;
	}
	return null;
}

function cargaContenido(idSelectOrigen)
{
	// Obtengo la posicion que ocupa el select que debe ser cargado en el array declarado mas arriba
	var posicionSelectDestino=buscarEnArray(listadoSelects, idSelectOrigen)+1;
	// Obtengo el select que el usuario modifico
	var selectOrigen=document.getElementById(idSelectOrigen);
	// Obtengo la opcion que el usuario selecciono
	var opcionSeleccionada=selectOrigen.options[selectOrigen.selectedIndex].value;

	// Si el usuario eligio la opcion "Elige", no voy al servidor y pongo los selects siguientes en estado "Selecciona opcion..."
	if(opcionSeleccionada==0)
	{
		var x=posicionSelectDestino, selectActual=null;
		// Busco todos los selects siguientes al que inicio el evento onChange y les cambio el estado y deshabilito
		while(listadoSelects[x])
		{
			selectActual=document.getElementById(listadoSelects[x]);
			selectActual.length=0;
			
			var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Selecciona Opci&oacute;n...";
			selectActual.appendChild(nuevaOpcion);	selectActual.disabled=true;
			x++;
		}
	}
	// Compruebo que el select modificado no sea el ultimo de la cadena
	else if(idSelectOrigen!=listadoSelects[listadoSelects.length-1])
	{
		// Obtengo el elemento del select que debo cargar
		var idSelectDestino=listadoSelects[posicionSelectDestino];
		var selectDestino=document.getElementById(idSelectDestino);
		// Creo el nuevo objeto AJAX y envio al servidor el ID del select a cargar y la opcion seleccionada del select origen
		var ajax=nuevoAjax();
		ajax.open("GET", "cractividad_proceso.php?select="+idSelectDestino+"&opcion="+opcionSeleccionada, true);		
		ajax.onreadystatechange=function() 
		{ 
			if (ajax.readyState==1)
			{
				// Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
				selectDestino.length=0;
				var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Cargando...";
				selectDestino.appendChild(nuevaOpcion); selectDestino.disabled=true;	
			}
			if (ajax.readyState==4)
			{
				selectDestino.parentNode.innerHTML=ajax.responseText;
			} 
		}
		ajax.send(null);
	}
}

function MostrarConsulta(datos)
{
	divResultado = document.getElementById('resultado');
	ajax2=nuevoAjax();
	ajax2.open("GET", datos);
	ajax2.onreadystatechange=function(){
		if (ajax2.readyState==4) 
		{
			divResultado.innerHTML = ajax2.responseText
		}
	}
	ajax2.send(null)
}
function enviarDatosActividad(id2, editar){
var i=0;
//donde se mostrará lo resultados
	divResultado = document.getElementById('resultado');
	//valores de los inputs
    crp=document.form1.cb_responsable.value;
	cme=document.form1.cb_metodologia.value;	
	cse=document.form1.ct_sesiones.value;		
	ato=document.form1.at_observa.value;
	id=document.form1.idpr.value;
	idtfbc=document.form1.tfbc.value;
	idtfbc2=document.form1.tfbc2.value;
	pr2=document.form1.pr.value;
    rbasig=getRadioButtonSelectedValue(document.form1.rb_asigna);

	if(crp=="0" || cse=="" || rbasig==undefined)
	{
	  alert("POR FAVOR DEBE LLENAR LOS CAMPOS OBLIGATORIOS y/o SELECCIONAR UNA ASIGNACION DE META");
  	}
	else
	{
	//instanciamos el objetoAjax
	ajax=nuevoAjax();
	//uso del medotod POST
	//archivo que realizará la operacion
	//registro.php
	ajax.open("POST", "cractividad_ins.php?mod="+editar+"&idmg="+id2 , true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divResultado.innerHTML = ajax.responseText
			//llamar a funcion para limpiar los inputs
			LimpiarCampos();
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
	ajax.send("cb_responsable="+crp+"&cb_metodologia="+cme+"&ct_sesiones="+cse+"&at_observa="+ato+"&tfbc="+idtfbc+"&tfbc2="+idtfbc2+"&rb_asigna="+rbasig+"&pr="+pr2+"&idpr="+id);
	}
}

function LimpiarCampos(){
//    document.form1.cb_pu.value="";
	document.form1.cb_responsable.value="0";
	document.form1.cb_metodologia.value="0";
	document.form1.ct_sesiones.value="";
	document.form1.at_observa.value="";
	document.form1.idpr.value="";	
	document.form1.rb_asigna.checked=false;	
    document.form1.cb_responsable.focus;		
}