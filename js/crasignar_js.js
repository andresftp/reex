function nuevoAjax()
{ 
	/* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
	var xmlhttp=false;
	try
	{
		// Creacion del objeto AJAX para navegadores no IE
		xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	}
	catch(e)
	{
		try
		{
			// Creacion del objet AJAX para IE
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		

		}
		catch(E)
		{
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') xmlhttp=new XMLHttpRequest();
		}
	}
	return xmlhttp; 
}

// Declaro los selects que componen el documento HTML. Su atributo ID debe figurar aqui.
var listadoSelects=new Array();
listadoSelects[0]="select1";
listadoSelects[1]="select2";
listadoSelects[2]="select3";
listadoSelects[3]="select4";
listadoSelects[4]="select5";
listadoSelects[5]="select6";
listadoSelects[6]="select7";
listadoSelects[7]="select8";
//listadoSelect[5]="select6";


function buscarEnArray(array, dato)
{
	// Retorna el indice de la posicion donde se encuentra el elemento en el array o null si no se encuentra
	var x=0;
	while(array[x])
	{
		if(array[x]==dato) return x;
		x++;
	}
	return null;
}

function cargaContenido(idSelectOrigen)
{
	// Obtengo la posicion que ocupa el select que debe ser cargado en el array declarado mas arriba
	var posicionSelectDestino=buscarEnArray(listadoSelects, idSelectOrigen)+1;
	// Obtengo el select que el usuario modifico
	var selectOrigen=document.getElementById(idSelectOrigen);
	// Obtengo la opcion que el usuario selecciono
	var opcionSeleccionada=selectOrigen.options[selectOrigen.selectedIndex].value;

	// Si el usuario eligio la opcion "Elige", no voy al servidor y pongo los selects siguientes en estado "Selecciona opcion..."
	if(opcionSeleccionada==0)
	{
		var x=posicionSelectDestino, selectActual=null;
		// Busco todos los selects siguientes al que inicio el evento onChange y les cambio el estado y deshabilito
		while(listadoSelects[x])
		{
			selectActual=document.getElementById(listadoSelects[x]);
			selectActual.length=0;
			
			var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Selecciona Opci&oacute;n...";
			selectActual.appendChild(nuevaOpcion);	selectActual.disabled=true;
			x++;
		}
	}
	// Compruebo que el select modificado no sea el ultimo de la cadena
	else if(idSelectOrigen!=listadoSelects[listadoSelects.length-1])
	{
		// Obtengo el elemento del select que debo cargar
		var idSelectDestino=listadoSelects[posicionSelectDestino];
		var selectDestino=document.getElementById(idSelectDestino);
		// Creo el nuevo objeto AJAX y envio al servidor el ID del select a cargar y la opcion seleccionada del select origen
		var ajax=nuevoAjax();
		ajax.open("GET", "crasignar_proceso.php?select="+idSelectDestino+"&opcion="+opcionSeleccionada, true);		
		ajax.onreadystatechange=function() 
		{ 
			if (ajax.readyState==1)
			{
				// Mientras carga elimino la opcion "Selecciona Opcion..." y pongo una que dice "Cargando..."
				selectDestino.length=0;
				var nuevaOpcion=document.createElement("option"); nuevaOpcion.value=0; nuevaOpcion.innerHTML="Cargando...";
				selectDestino.appendChild(nuevaOpcion); selectDestino.disabled=true;	
			}
			if (ajax.readyState==4)
			{
				selectDestino.parentNode.innerHTML=ajax.responseText;
			} 
		}
		ajax.send(null);
	}
}

function MostrarConsulta(datos)
{
	divResultado = document.getElementById('resultado');
	ajax=objetoAjax();
	ajax.open("POST", datos);
	ajax.onreadystatechange=function(){
		if (ajax.readyState==4) 
		{
			divResultado.innerHTML = ajax.responseText
		}
	}
	ajax.send(null)
}

function enviarDatosMetaGlobal(editar){
	//donde se mostrará lo resultados
	divResultado = document.getElementById('resultado');
	//valores de los inputs
    cpu=document.form1.select1.value;
	cpr=document.form1.select2.value;
	se8=document.form1.select8.value;
	caf=document.form1.cb_afiscal.value;
	cmt=document.form1.ct_meta.value;	
	cme=document.form1.cb_medicion.value;		
	ato=document.form1.at_observa.value;
	validmg=document.form1.idmg.value;
	tipo2=document.form1.tipo.value;
	

	if(cpu=="0" || cpr=="0" || se8=="0" || caf=="0" || cmt=="" || cme=="0")
	{
	  alert("OJO DEBE LLENAR LOS CAMPOS OBLIGATORIOS");
	}
	else
	{
	//instanciamos el objetoAjax
	ajax=nuevoAjax();
	//uso del medotod POST
	//archivo que realizará la operacion
	//registro.php
	ajax.open("POST", "crasignar_ins.php?mod="+editar,true);
	ajax.onreadystatechange=function() {
		if (ajax.readyState==4) {
			//mostrar resultados en esta capa
			divResultado.innerHTML = ajax.responseText
			//llamar a funcion para limpiar los inputs
			LimpiarCampos();
		}
	}
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	//enviando los valores
	ajax.send("select1="+cpu+"&select2="+cpr+"&select8="+se8+"&cb_afiscal="+caf+"&ct_meta="+cmt+"&cb_medicion="+cme+"&tipo="+tipo2+"&at_observa="+ato+"&idmg="+validmg);
	}
}

function LimpiarCampos(){
//    document.form1.cb_pu.value="";
	document.form1.select1.value="0";
	document.form1.select2.value="0";
	document.form1.select3.value="0";
	document.form1.select4.value="0";
	document.form1.select5.value="0";	
	document.form1.select6.value="0";		
	document.form1.select7.value="0";			
	document.form1.select8.value="0";			
	document.form1.cb_afiscal.value="0";
	document.form1.ct_meta.value="";	
	document.form1.cb_medicion.value="0";		
	document.form1.at_observa.value="";	
    document.form1.select1.focus;		
}