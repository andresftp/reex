$(document).ready(function() {
	    if  (!("Notification"  in  window))  {
	        alert("Este navegador no soporta notificaciones de escritorio");
	    }
	    else  if  (Notification.permission  ===  "granted")  {
			$.ajax({
				data:  {proceso:1},
				dataType: 'json',
				url:   '../notificaciones/notificaciones.php',
				type:  'post',
				success:  function (response) {
					var options  =   {
			            body:   response.mensaje,
						icon:   "../img/reex.PNG",
			            dir :   "ltr"
					};
					var num_eventos = "Tienes "+response.num_eventos+" actividades nuevas";
			        var notification  =  new  Notification(num_eventos, options);
			        setTimeout(function(){ notification.close(); },12000);
				}
			});
	    }
	    else  if  (Notification.permission  !==  'denied')  {
	        Notification.requestPermission(function (permission)  {
	            if  (!('permission'  in  Notification))  {
	                Notification.permission  =  permission;
	            }
	            if  (permission  ===  "granted")  {
	                var  options  =   {
	                	body:   "Permiso concedido para sistema de notificaciones",
			        	icon:   "../img/reex.PNG",
			            dir :   "ltr"
	                };
	                var  notification  =  new  Notification("Notificaciones Reex", options);
	                setTimeout(function(){ notification.close(); },12000);
	            }
	        });
	    }
});
